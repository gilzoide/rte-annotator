#!/bin/sh

pot_file=messages.pot

mostra_uso() {
	echo "USO: $0 ( init <linguagem> | update | compile )"
	exit -1
}

case "$1" in
	init)
		if [[ $2 = "" ]]; then
			echo -e "É necessário definir a linguagem:\n"
			mostra_uso
		fi
		pybabel extract -F babel.cfg -k lazy_gettext -o messages.pot rteannotator
		pybabel init -i messages.pot -d rteannotator/translations -l $2
		;;
	update)
		pybabel extract -F babel.cfg -k lazy_gettext -o messages.pot rteannotator
		pybabel update -i messages.pot -d rteannotator/translations
		;;
	compile)
		pybabel compile -d rteannotator/translations
		;;
	*)
		mostra_uso
		;;
esac

exit
