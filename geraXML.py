#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Gera um arquivo XML de CompTexts para submissão no sistema.
'''

import sys
import xml.etree.ElementTree as ET
from glob import glob
import re
from os import path

acha_banda_re = r"banda_(\d)"

def main():
    if len(sys.argv) < 2:
        print('É necessário nome da pasta!')
        return -1
    pasta = sys.argv[1]
    arquivos = glob(path.join(pasta, '*/*.txt'))

    root = ET.Element('text-complexity-corpus')
    for nome in arquivos:
        # complexidade (banda)
        m = re.search(acha_banda_re, nome)
        complexidade = m.group(1)
        # número de palavras
        num_palavras = int(path.basename(nome)[:4])

        texto = ET.SubElement(root, 'text')
        compl = ET.SubElement(texto, 'complexity-level')
        compl.text = complexidade
        wordcount = ET.SubElement(texto, 'word-count')
        wordcount.text = str(num_palavras)
        content = ET.SubElement(texto, 'content')
        with open(nome, 'r') as arq:
            content.text = arq.read().decode('utf-8')
    print(ET.tostring(root, 'utf-8'))

if __name__ == '__main__':
    sys.exit(main())
