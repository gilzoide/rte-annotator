# -*- coding: utf-8 -*-

'''
This module contains classes for the entities representing in the
database.
'''

from rteannotator import db
from flask_login import current_user

DESIRED_ANNOTATIONS_PER_PAIR = 4

class Annotator(db.Model):
    '''
    Class mapping to the Annotator in the DB.
    Represents one person that annotates the data.
    '''
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column(db.String(50), index=True, unique=True)
    password = db.Column(db.String(128))
    salt = db.Column(db.String(128))
    show_all_pairs = db.Column(db.Boolean, default=True)
    type = db.Column(db.Integer, default=1)
    test = db.Column(db.Boolean, default=False)
    
    # all individual annotations (i.e. annotated pairs) by this user 
    rte_annotations = db.relationship('RTEAnnotation', backref='annotator',
                                      lazy='dynamic')
    comptex_annotations = db.relationship('CompTextAnnotation', backref='annotator',
                                          lazy='dynamic')
    
    user_types = {1: 'Normal',
                  2: 'Expert',
                  3: 'Admin'}
    
    def __repr__(self):
        return '<Annotator {}>'.format(self.name)
        
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def is_admin(self):
        return self.type == 3

    def have_special_permissions(self):
        return self.type > 1
    
    def may_submit_file(self):
        return self.type == 3
    
    def may_edit_pair(self):
        '''Only expert/admins can edit pairs'''
        return self.type > 1
    
    def may_download_raw_file(self):
        return self.type == 3
    
    def may_download_annotated_file(self):
        return self.type == 3
    
    def may_create_user(self):
        return self.type == 3
    
    def may_edit_user(self):
        return self.type == 3
    
    def may_see_all_statistics(self):
        return self.type > 1
    
    def may_edit_file(self):
        return self.type == 3
    
    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3
    
class File(db.Model):
    '''
    Class representing a file containing many TE examples.
    '''
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column(db.String(50), index=True, unique=True)
    vsm = db.Column(db.String(10))

    # Type of file: RTE or CompText (text complexity)
    type = db.Column(db.Enum('rte', 'comptext'))

    # Is this the file just for tests?
    test = db.Column(db.Boolean)

    available = db.Column(db.Boolean, default=True)
    # RTE pairs
    pairs = db.relationship('Pair', backref='file', lazy='dynamic')
    # CompTexts
    comptexts =  db.relationship('CompText', backref='file', lazy='dynamic',
                                 cascade="all")
    
    def __repr__(self):
        return '<File {}>'.format(self.name)
    
    def __init__(self, name, type, vsm=None):
        self.type = type
        self.name = name
        self.vsm = vsm
    
    def load_metadata(self):
        '''
        Load some metadata to the File object. The data are meant to be 
        accessed by the templates, and include statistics over the annotation.
        '''
        from rteannotator.rte.model import utils
        annotation_query = utils.get_file_annotation_query(self.id)
        annotations = annotation_query.all()
        
        self.num_annotations = len(annotations)
        self.num_annotated_by_user = len([a for a in annotations
                                          if a.annotator_id == current_user.id])
        
        # count pairs with enough annotations
        self.num_annotated_enough = utils.count_annotated_enough(annotations)
        
        # count reviewed pairs
        self.num_reviewed_and_kept = utils.count_reviewed_and_kept(self.id)
        self.num_reviewed_by_user = utils.count_reviewed_by_user(self.id)

