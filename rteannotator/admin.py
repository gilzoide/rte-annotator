# -*- coding: utf-8 -*-

'''
Admin page, with basic CRUD for DB
'''

from flask import redirect, url_for, request
from flask_admin import Admin, BaseView, expose, AdminIndexView
from flask_admin.contrib.sqla import ModelView
from flask_login import current_user

import model
from rteannotator import db
import rteannotator.comptext as comptext


class MyBaseView(BaseView):
    '''Base view that needs admin user to work'''
    def is_accessible(self):
        return current_user.is_authenticated and current_user.may_edit_user()

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user doesn't have access
        return redirect(url_for('login', next=request.url))

class MyIndexView(MyBaseView, AdminIndexView):
    '''Index view that needs admin user to wok'''
    pass

class MyModelView(ModelView, MyBaseView):
    '''Model view that needs admin user to wok'''
    pass

class AnnotatorModelView(MyModelView):
    column_filters = ('name', 'type', 'test')

class TextModelView(MyModelView):
    '''Model view that let user filter texts by complexity'''
    column_filters = ('id', 'title', 'complexity_level', 'file.test')
    page_size = 25

class AnnotationModelView(MyModelView):
    column_filters = ('complexity', 'pair_id', 'annotator.name', 'annotator.test', 'pair.test')

class PairModelView(MyModelView):
    column_filters = ('id', 'text1_id', 'text2_id', 'test')

class PendingModelView(MyModelView):
    column_filters = ('annotator.name', 'annotator_id', 'pair_id')

class TeacherInfoModelView(MyModelView):
    column_filters = ('annotator_id', 'full_name', 'school', 'cicle', 'grade')


def setup_admin(app, **kwargs):
    admin = Admin(app, index_view=MyIndexView(), **kwargs)
    # CRUD views
    admin.add_view(MyModelView(model.File, db.session))
    admin.add_view(AnnotatorModelView(model.Annotator, db.session))
    admin.add_view(TeacherInfoModelView(comptext.model.TeacherInfo, db.session))
    admin.add_view(TextModelView(comptext.model.CompText, db.session, endpoint='comptextmodel'))
    admin.add_view(PairModelView(comptext.model.CompTextPair, db.session))
    admin.add_view(AnnotationModelView(comptext.model.CompTextAnnotation, db.session))
    admin.add_view(PendingModelView(comptext.model.PendingCompTextAnnotation, db.session))
    return admin

