��    Q      �              ,     -     6     H  	   Y  
   c     n     s     z  !   �     �     �     �     �     �          *     2     B     O     X     a     u     �     �     �     �     �     �     �     �     �          	  #     	   :  9   D     ~     �     �     �  $   �     �     �  
             %     .     L     \  /   u  	   �  
   �     �  
   �     �     �     �     	     	     	  *   	  )   H	  '   r	  )   �	  '   �	  )   �	  `   
  #   w
     �
     �
     �
     �
  
   �
  
   �
  
   �
  "   �
  
             !     7  �  <     �     �     �     �  
                    %   '  )   M     w     �     �  +   �     �     �     �     
          $     -     A     _     d     j     v  %   �     �     �     �     �     �  
   �  +   �     +  F   <  (   �     �     �     �  $   �               )     7     =     C     ^     n  ,   �  	   �     �     �     �     �               0     I     O  5   U  1   �  .   �  1   �  .     &   M  O   t  #   �     �     �                 	   '  
   1  %   <     b     w     �     �   Annotate Annotate new pair Annotation hints Annotator Annotators Back Change Change password Choose which text is more complex Classifier complexity level Click here to log in Comment CompText Annotations Complexity annotation required Confirm password Contact Create new user Developed by Examples Expected Expected Annotation File submited successfully Given Hints How to annotate Linguist Complexity Linguist complexity level Log out Logged in as Login My annotations Name New password New password not confirmed properly Next page No pairs fully annotated by only the selected annotators. No pairs fully annotated. Number of annotations Number of comments Old password Optional comment for your annotation Pair Pair annotated successfully Pair texts Pairs Password Password successfully changed RTE Annotations See annotator statistics See dataset statistics from selected annotators See pairs Select all Special permissions Statistics Submit Submit XML file Teacher Annotation Teacher Comment Test Text Text 1 and text 2 have the same complexity Text 1 is little more complex than text 2 Text 1 is much more complex than text 2 Text 2 is little more complex than text 1 Text 2 is much more complex than text 1 Thank you very much for your contribution There already exists a file with the same name. Are you sure this has not been submitted before? There are no more pairs to annotate Total Unselect all User View annotators View files View pairs View texts Welcome to the annotation platform Word count Wrong password You are not logged in test Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2017-10-11 14:57-0300
PO-Revision-Date: 2017-10-11 14:38-0300
Last-Translator: gilzoide <gilzoide@gmail.com>
Language: pt
Language-Team: pt <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.4.0
 Anotar Anote novo par Dicas de anotação Anotador Anotadores Voltar Trocar Trocar senha Escolha qual é o texto mais complexo Nível de complexidade pelo classificador Clique aqui para conectar Comentário Anotações CompText É necessária a anotação de complexidade Confirmar senha Contato Criar novo usuário Desenvolvido por Exemplos Esperado Anotação esperada Arquivo submetido com sucesso Dado Dicas Como anotar Complexidade pelo linguista Nível de complexidade pelo linguista Desconectar Conectado como Conectar Minhas anotações Nome Nova senha Nova senha não foi corretamente confirmada Próxima página Não há pares suficientemente anotados pelos anotadores selecionados. Não há pares suficientemente anotados. Número de anotações Número de comentários Senha antiga Comentário opcional para anotação Par Par anotado com sucesso Parear Textos Pares Senha Senha alterada com sucesso Anotações RTE Ver estatísticas de anotadores Ver estatísticas de anotadores selecionados Ver pares Selecionar todos Permissões especiais Estatísticas Submeter Submeter arquivo XML Anotação do professor Comentário do professor Teste Texto O texto 1 e o texto 2 apresentam a mesma complexidade O texto 1 é um pouco mais complexo que o texto 2 O texto 1 é muito mais complexo que o texto 2 O texto 2 é um pouco mais complexo que o texto 1 O texto 2 é muito mais complexo que o texto 1 Muito obrigado pela sua contribuição Já existe um arquivo com esse nome. Você tem certeza que já não o submeteu? Não há mais pares para anotação Total Desselecionar todos Usuário Ver anotadores Ver arquivos Ver pares Ver textos Bem-vindo à plataforma de anotação Contagem de palavras Senha inválida Você não está conectado teste 