# -*- coding: utf-8 -*-

import os
import math
import flask
from xml.etree import cElementTree as ET
from collections import OrderedDict
from urlparse import urlparse, urljoin

from rteannotator import model
from rteannotator import app

DATABASE = 'rte.db'

def find_num_pages(num_items, items_per_page):
    '''
    Compute and return the number of pages to display all items.
    '''
    division = num_items / float(items_per_page)
    return int(math.ceil(division))

def paginate_query(query, page, items_per_page):
    '''
    Return a query with the results corresponding to the given page.
    Page numbers start from 1!
    '''
    index_from = (page - 1) * items_per_page
#     index_until = page * items_per_page
    
    return query.offset(index_from).limit(items_per_page)

def get_items_in_page(items, page, items_per_page):
    '''
    Return a sublist within an item list corresponding to the given page.
    Page numbers start from 1!
    '''
    index_from = (page - 1) * items_per_page
    index_until = page * items_per_page
    
    return items[index_from : index_until]

def get_upload_folder():
    '''
    Return the upload folder
    '''
    return app.config['UPLOAD_FOLDER']

def check_filename(filename):
    '''
    Check whether a given filename does not include directories.
    Used to ensure it is no attempt to get some other file from the server.
    Return True if filename is ok.
    '''
    basename = os.path.basename(filename)
    return basename == filename 

def load_pairs_from_xml(xml_file):
    '''
    Load and return the RTE pairs from a XML file.
    '''
    tree = ET.parse(xml_file)
    root = tree.getroot()
    assert root.tag == 'entailment-corpus'
    
    # we implement the set of pairs as an ordered dict
    # so we can easily access pairs by id and they are always in
    # the same order.
    pairs = OrderedDict()
    annotated = 0
    for xml_pair in root:
        pair = model.Pair(xml_pair)
        pairs[pair.id] = pair
        if pair.entailment != 'UNKNOWN':
            annotated += 1
    
    filename = os.path.basename(xml_file)
    file_data = model.FileData(filename, pairs, annotated)
    
    return file_data

def num_or_na(num):
    '''
    Simple function to return the number itself or the string 'NA'
    if it is None. 
    '''
    return num if num is not None else 'NA'

def get_uploaded_files():
    '''
    Return a list of uploaded files.
    '''
    return os.listdir(get_upload_folder())

def get_pairs_key():
    '''
    Return the key used in the cache to get the RTE pairs
    for the current user.
    '''
    username = flask.session['username']
    key = 'pairs-{}'.format(username)
    return key


def is_safe_url(target):
    '''
    Checks if a URL is safe: from this same domain
    '''
    request = flask.request
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc

def redirect_back(endpoint, **values):
    '''Redirect user back to "next" URL or the referrer'''
    request = flask.request
    target = request.form.get('next') or request.referrer
    if not target or not is_safe_url(target):
        target = url_for(endpoint, **values)
    return flask.redirect(target)
