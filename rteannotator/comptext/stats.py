# -*- coding: utf-8 -*-

import numpy as np
import rpy2.robjects as ro
from rpy2.robjects import numpy2ri
from rpy2.robjects.packages import importr

def complexity_level_for_pairs(pairs):
    '''
    Calculate the complexity level for pairs, based on the complexity level
    of each text individualy.

    :param pairs: Iterable of 2-Tuple with the complexity level for individual
                  texts E [1, 4]

    :return: Numpy array with complexity level for each pair in the same order as passed
    '''
    pairs = np.array(pairs)
    # diff \in [-3, 3]
    diff = pairs[:,1] - pairs[:,0]
    # clip diff to [-2, 2]
    np.clip(diff, -2, 2, out=diff)
    # shift from [-2, 2] to [1, 5]
    complexity_values = diff + 3

    return complexity_values

def complexity_level_for_comptext_pairs(pairs):
    '''
    Calculate the complexity level for CompTextPairs, based on the complexity
    level of each text individualy.

    :param pairs: Iterable of CompTextPairs

    :return: Numpy array with complexity level for each pair in the same order as passed
    '''
    return complexity_level_for_pairs([(p.text1.complexity_level, p.text2.complexity_level) for p in pairs])

def fleiss_kappa(mat):
    '''
    Calculate the Fleiss' Kappa

    :param mat: Numpy Matrix of 'Subjects x Categories', shape (number of subjects, number of categories)
                Rows    - Assesed Pairs (Pair of id X...)
                Columns - Assessments   (LME, LSE, ED, RSE, RME)

    :result: Fleiss Kappa 
    '''
    def check_line_count(m):
        '''
        Assert that every pair have the expected number of annotations (5)

        :return: Number of assessments per subject
        '''
        sums = np.sum(m, axis=1)
        assert reduce(lambda acc, x: x if acc == x else None, sums), \
               "All columns should have the same amount of assessments: " + str(sums)
        return sums[0]
    n = check_line_count(mat)
    N, K = mat.shape
    p = np.sum(mat, axis=0, dtype=float) / (n * N)
    Pi = (np.sum(np.power(mat, 2), axis=1, dtype=float) - n) / (n * (n - 1))
    P = np.sum(Pi) / N
    Pe = np.sum(np.power(p, 2))
    return (P - Pe) / (1 - Pe)

def r_kappa(mat):
    '''Fleiss' Kappa from R, to check if my implementation is right'''
    #  mat = np.clip(mat, 2, 4)
    numpy2ri.activate()
    irr = importr("irr")
    print irr.kappam_fleiss(mat)


def gamma(mat):
    '''
    Calculate the Gamma correlation using the Cross Tabulation of Assessments.

    :param mat: NxN matrix with frequency of cross assessments
    '''
    N, M = mat.shape
    assert N == M, "Matrix for Gamma correlation should be NxN, not " + str((N, M))
    def contribution_to_Na(i, j):
        return mat[i, j] * np.sum(mat[i + 1:, j + 1:])
    Na = np.sum((contribution_to_Na(i, j) for i in range(N) for j in range(N)))

    def contribution_to_Ni(i, j):
        return mat[i, j] * np.sum(mat[i + 1:, :j])
    Ni = np.sum((contribution_to_Ni(i, j) for i in range(N) for j in range(N - 1, -1, -1)))

    try:
        return float(Na - Ni) / float(Na + Ni)
    except ZeroDivisionError:
        return np.nan


def icc(mat):
    numpy2ri.activate()
    irr = importr("irr")
    res = irr.icc(mat, model="oneway", type="consistency", unit="single")
    #  print res
    return {n: res.rx(n)[0][0] for n in res.names}

