# -*- coding: utf-8 -*-

from sqlalchemy.orm import aliased
from sqlalchemy.sql import func
from flask_login import current_user
from flask_babel import gettext, lazy_gettext
from sklearn.metrics import cohen_kappa_score
import rpy2.robjects as ro

from rteannotator import app, db
from rteannotator.model import File, Annotator
from model import *
from stats import *

import re
from itertools import chain, groupby, product, combinations, tee, izip
from pprint import pprint
import math
from collections import Counter

BATCH_SIZE = 25


def get_text_batch(initial_page=0):
    '''
    Get texts in a batch of BATCH_SIZE
    '''
    first_text = initial_page * BATCH_SIZE
    return CompText.query.filter(CompText.id > first_text).limit(BATCH_SIZE).all()


def get_text(text_id):
    return CompText.query.filter_by(id=text_id).first()


def set_size_interval(texts, nbins):
    '''
    Puts where in the word count interval texts are, separed from 1 (less words)
    to ``nbins`` (more words)
    '''
    factor = float(nbins) / len(texts)
    for i, t in enumerate(texts):
        t.size_interval = math.ceil((i + 1) * factor)


def pair_texts_by_size(texts, nbins, tuple_only=False):
    '''
    Generate the CompTextPairs from CompTexts

    :param tuple_only: Flag to not create the Pair DBwise, only return the tuples
    '''
    def get_interval(t):
        return t.size_interval
    set_size_interval(texts, nbins)
    by_size = [list(g) for k, g in groupby(texts, get_interval)]

    pairs = [CompTextPair(t1, t2) if tuple_only is False else (t1, t2)
             for batch in by_size
             for t1, t2 in combinations(batch, 2)]
    return pairs


def partition(items, predicate=bool):
    '''
    Partition an iterable by the predicate, returning two iterables: one for 
    which predicate is true, and one for which it is false
    '''
    a, b = tee((predicate(item), item) for item in items)
    return ((item for pred, item in a if pred),
            (item for pred, item in b if not pred))


def pair_texts_by_word_count(texts, factor, tuple_only=False):
    '''
    Generate the CompTextPairs from CompTexts, pairing only if texts sizes are
    similar: 1 / factor <= size1 / size2 <= factor

    :param tuple_only: Flag to not create the Pair DBwise, only return the tuples

    :return: Iterable of pairs that match the factor
    :return: Iterable of pairs that DON'T match the factor
    '''
    if factor < 1:
        inv_factor, factor = factor, 1.0 / factor
    else:
        inv_factor = 1.0 / factor

    def pred(t):
        return inv_factor <= float(t[0].word_count) / float(t[1].word_count) <= factor
    pairs4annotation, pairs4test = partition(combinations(texts, 2), pred)

    return ([CompTextPair(t1, t2, test=False) if tuple_only is False else (t1, t2) for t1, t2 in pairs4annotation],
            [CompTextPair(t1, t2, test=True) if tuple_only is False else (t1, t2) for t1, t2 in pairs4test])


def non_test_annotations(pair_or_annotator):
    '''
    Get the annotations for a pair or annotator that are not test.
    If it is a pair, filter test annotators. If it is an annotator, filter test pairs.
    '''
    if isinstance(pair_or_annotator, CompTextPair):
        return [a for a in pair_or_annotator.annotations if a.annotator.test != True]
    else:
        return [a for a in pair_or_annotator.comptex_annotations if a.pair.test != True]

def get_pair_for_annotation():
    '''
    Get a pair of texts for annotation
    '''
    if current_user.pending_comptext_annotations:
        # pending annotations may already have been annotated
        # DESIRED_ANNOTATIONS_PER_PAIR times, so delete if any of these appear
        for pending in current_user.pending_comptext_annotations:
            if len(non_test_annotations(pending.pair)) >= DESIRED_ANNOTATIONS_PER_PAIR:
                db.session.delete(pending)
            else:
                db.session.commit()
                return pending
    # either there are no pending annotations, or all pending annotations were
    # actually already annotated 4 times. Reserve more, if possible
    app.logger.warning("[User: {}] Reserving more pairs".format(current_user.id))
    return reserve_pairs(current_user)


def get_pair_for_tutorial():
    _annotation = aliased(CompTextAnnotation)
    already_seen = CompTextPair.query.join(_annotation, CompTextPair.annotations) \
                                     .filter(_annotation.annotator_id == current_user.id)
    return CompTextPair.query.join(CompTextPair.text1) \
                             .except_(already_seen) \
                             .filter(CompTextPair.test == True) \
                             .order_by(func.random()) \
                             .first()


def reserve_pairs(annotator):
    '''
    Reserve some CompText pairs for annotator
    '''
    first_pending_annotation = None
    for p in get_pairs_for_reservation(annotator):
        pending_annotation = PendingCompTextAnnotation(annotator, p)
        first_pending_annotation = first_pending_annotation or pending_annotation
        db.session.add(pending_annotation)
    db.session.commit()
    return first_pending_annotation


def get_pairs_for_reservation(annotator):
    '''
    Get pairs for annotation, prioritizing the ones with texts Annotator didn't
    yet meet and those that already have 3 annotations.

    Duplicate texts are removed.

    :return: Iterable of selected pairs
    '''
    _annotation = aliased(CompTextAnnotation)
    _annotator = aliased(Annotator)
    already_seen = CompTextPair.query.join(_annotation, CompTextPair.annotations) \
                                     .filter(_annotation.pair_id == current_user.id)
    fully_annotated = get_fully_annotated_pairs(query=True)

    pairs = CompTextPair.query.join(CompTextPair.text1) \
                              .filter(CompTextPair.test != True) \
                              .except_(fully_annotated) \
                              .except_(already_seen) \
                              .order_by(func.random()) \
                              .all()
    return remove_duplicate_texts(pairs)
    

def remove_duplicate_texts(pairs):
    '''
    After grabbing the pairs for reservation, filter those who repeat a text.

    Thanks, Paul Bardes, for helping me with this one =]
    '''
    chosen_texts = set()
    for p in pairs:
        if not(p.text1_id in chosen_texts or p.text2_id in chosen_texts):
            chosen_texts.add(p.text1_id)
            chosen_texts.add(p.text2_id)
            yield p


def htmlify_text(t, adminView=False):
    '''
    Clean a text for displaying in html.

    :param adminView: Admin view shows more information that annotators
                      shouldn't see
    '''
    text = t.text
    text = text.strip()
    text = text.replace('\n\n', '<br /><br />')
    #  text = re.sub(r'(.+)', r'<p>\1</p>', text, flags=re.M)
    text = re.sub(r'<title>(.+?)</title>', r'<h2>\1</h2>', text)
    text = re.sub(r'<subtitle>(.+?)</subtitle>', r'<h3>\1</h3>', text)

    # Admin infooooooo
    if current_user.is_admin() and adminView:
        text = u"<i>({}: {}; {}: {}; {}: {})</i><br />{}".format(lazy_gettext('Linguist complexity level'),
                                                                 t.complexity_level,
                                                                 lazy_gettext('Word count'),
                                                                 t.word_count,
                                                                 lazy_gettext('Classifier complexity level'),
                                                                 t.classifier_level,
                                                                 text)
    return text

app.jinja_env.filters['htmlify'] = htmlify_text

def annotate_pair(pair_id, complexity, comment):
    '''
    Annotate a text pair
    '''
    pair = CompTextPair.query.filter_by(id=pair_id).first()
    assert pair, "[User: {}] Pair {} was not found!".format(current_user.id, pair_id)
    if pair.test != True:
        pending = next((p for p in current_user.pending_comptext_annotations
                        if p.pair.id == pair_id), None)
        assert pending is not None, \
               "[User: {}] PendingAnnotation not registered for user: pair {}".format(current_user.id, pair_id)
        db.session.delete(pending)
    annotation = CompTextAnnotation(current_user, pair, complexity, comment)
    db.session.add(annotation)
    db.session.commit()


def count_annotations(users):
    '''Count the amount of annotations for each user, no-test only'''
    count = db.session.query(CompTextAnnotation.annotator_id, func.count(CompTextAnnotation.annotator_id)) \
                      .join(CompTextPair) \
                      .group_by("annotator_id") \
                      .filter(CompTextPair.test != True) \
                      .all()
    count = dict(count)
    for user in users:
        user.comptext_annotation_count = count.get(user.id, 0)

def get_non_test_pair_count():
    '''Retrieve the count of non-test pairs'''
    return CompTextPair.query.filter(CompTextPair.test != True).count()

def get_non_test_pairs():
    '''Retrieve all non-test pairs'''
    return CompTextPair.query.filter(CompTextPair.test != True).all()

def get_fully_annotated_pairs(query=False):
    '''
    Retrieve the pairs that were fully annotated

    :type query: bool
    :param query: Return SQLAlchemy query instead of pairs?

    :return: Fully annotated pairs
    '''
    _annotation = aliased(CompTextAnnotation)
    _annotator = aliased(Annotator)
    fully_annotated_q = CompTextPair.query.join(_annotation, CompTextPair.annotations) \
                                          .join(_annotator, Annotator) \
                                          .filter(_annotator.test != True) \
                                          .group_by(_annotation.pair_id) \
                                          .having(func.count(_annotation.pair_id) == DESIRED_ANNOTATIONS_PER_PAIR)
    return fully_annotated_q if query else fully_annotated_q.all()


def get_complexities(pair):
    return [a.complexity for a in non_test_annotations(pair)]


def kappa_for_pairs(pairs):
    '''
    Calculate the Fleiss' Kappa for the given pairs.

    :return: Kappa for the Teachers + Individual Complexity Level
    :return: Kappa for only the Teachers
    '''
    complexity_level = complexity_level_for_comptext_pairs(pairs)

    #  mat = np.column_stack((np.array([get_complexities(p) for p in pairs]),
                           #  complexity_level))
    #  r_kappa(mat)

    def generate_line(i, p):
        '''
        Generate a line on the expected matrix based on a pair p (pairs[i])

        :return: Line for Teachers + Individual Complexity Level
        :return: Line for Teachers only
        '''
        ret = np.zeros(5, dtype=int)
        ret_t = np.zeros(5, dtype=int)
        ret[complexity_level[i] - 1] += 1
        for a in non_test_annotations(p):
            ret[a.complexity - 1] += 1
            ret_t[a.complexity - 1] += 1
        return ret, ret_t
    mat = np.array([generate_line(i, p) for i, p in enumerate(pairs)])
    return fleiss_kappa(mat[:,0]), fleiss_kappa(mat[:,1])

def kappa_aggregating_values(pairs):
    complexity_level = complexity_level_for_comptext_pairs(pairs)
    def get_index(x):
        '''Transform [1, 5] to [0, 1]'''
        x = np.clip(x, 2, 3)
        return x - 2
    def generate_line(i, p):
        '''
        Generate a line on the expected matrix based on a pair p (pairs[i])

        :return: Line for Teachers + Individual Complexity Level
        :return: Line for Teachers only
        '''
        ret = np.zeros(2, dtype=int)
        ret_t = np.zeros(2, dtype=int)
        ret[get_index(complexity_level[i])] += 1
        for a in non_test_annotations(p):
            ret[get_index(a.complexity)] += 1
            ret_t[get_index(a.complexity)] += 1
        return ret, ret_t
    mat = np.array([generate_line(i, p) for i, p in enumerate(pairs)])
    return fleiss_kappa(mat[:,0]), fleiss_kappa(mat[:,1])



POSSIBLE_ANNOTATIONS = len(CompTextAnnotation.value_map)
def get_majority(pairs):
    '''
    Filter the pairs that have at least 3 agreeing annotations
    :return: A 2-tuple with lists for the filtered pairs and the majoritary vote
    '''
    def do_agree(p):
        c = Counter(get_complexities(p))
        compl, count = c.most_common(1)[0]
        return compl if count > DESIRED_ANNOTATIONS_PER_PAIR / 2 else False
    def pair_plus_complexity(p):
        return p, do_agree(p)
    pairs_compl = np.array([(p, compl)
                            for p, compl in map(pair_plus_complexity, pairs)
                            if compl != False])
    return pairs_compl[:,0], pairs_compl[:,1]

def confusion_for_majoritary_voted():
    '''
    Get the confusion matrix for fully annotated pairs with majoritary votes
    (at least 3 agreeing annotations)
    '''
    pairs = get_fully_annotated_pairs()
    #  pairs = get_non_test_pairs()
    pairs, complexities = get_majority(pairs)

    size = POSSIBLE_ANNOTATIONS + 1
    mat = np.zeros((size, size), dtype=np.float)
    for expected, found in izip(complexity_level_for_comptext_pairs(pairs), complexities):
        mat[expected - 1, found - 1] += 1

    # calculate the sums
    mat[:-1,-1] = np.sum(mat[:-1,:-1], axis=1)
    mat[-1] = np.sum(mat[:-1], axis=0)

    return mat


def gamma_for_annotator(annotator):
    '''Get the Gamma correlation for an annotator'''
    annotations = non_test_annotations(annotator)
    annotated_pairs = (a.pair for a in annotations)
    size = POSSIBLE_ANNOTATIONS
    mat = np.zeros((size, size), dtype=np.float)
    for expected, found in izip(complexity_level_for_comptext_pairs(annotated_pairs),
                                (a.complexity for a in annotations)):
        mat[expected - 1, found - 1] += 1
    # flip the matrix view, so that the top-right corner is 5x5
    mat = mat[::-1, ::-1]
    return gamma(mat), len(annotations)


def get_num_annotations(real_name=None):
    '''
    Get the Number of Annotations for all annotators

    :param real_name: Should name be the real annotator name, or "Name {i}"?

    :return: {
        "table": gamma table for interface
        "total": total number of annotations
    }
    '''
    def find_number_of_annotations_and_comments(a):
        """Find how many annotations there is for a Teacher, and how many comments"""
        annotations = non_test_annotations(a)
        return len(annotations), len(filter(lambda x: bool(x.comment), annotations))
    result = np.array([(a,) + find_number_of_annotations_and_comments(a)
                       for i, a in enumerate(Annotator.query.filter(Annotator.test != True).all())])
    num_annotations = np.sum(result[:,1].astype(int))
    num_comments = np.sum(result[:,2].astype(int))
    return {
        'table': np.concatenate((result, [["Total", num_annotations, num_comments]]), axis=0),
        'total': num_annotations,
    }

def get_icc(pairs, *args, **kwargs):
    """Get the ICC for the pairs + any additional columns (they must have same dimension)"""
    complexities = np.array(map(get_complexities, pairs))
    mat = np.column_stack((complexities,) + args)
    # Transform [1, 5] to [2, 4], aggregating LME and LSE; RME and RSE
    if kwargs.get("aggregate_12_45"):
        np.clip(mat, 2, 4, out=mat)
    return icc(mat)

def get_icc_with_na(pairs):
    """Get the ICC for the pairs, filling matrix with NAs where needed"""
    def fill_line(annotations):
        i = 0
        for a in annotations[:DESIRED_ANNOTATIONS_PER_PAIR]:
            i += 1
            yield a
        while i < DESIRED_ANNOTATIONS_PER_PAIR:
            i += 1
            yield ro.NA_Integer
    complexities = map(get_complexities, pairs)
    in_vector = ro.IntVector(list(chain.from_iterable((fill_line(c) for c in complexities))))
    mat = ro.r.matrix(in_vector, nrow=len(pairs), ncol=DESIRED_ANNOTATIONS_PER_PAIR, byrow=True)
    return icc(mat)

def get_non_test_annotators():
    """
    Get the Annotators that are not marked as Test that have at least one
    CompTextAnnotation
    """
    #  _annotation = aliased(CompTextAnnotation)
    return Annotator.query.join(Annotator.comptex_annotations) \
                          .filter(Annotator.test != True) \
                          .group_by(Annotator.id) \
                          .having(func.count(Annotator.comptex_annotations) > 0) \
                          .all()

def get_annotator_info_full(annotator_id):
    """
    Get information about an Annotator if annotator_id is supplied, or all of
    them otherwise
    """
    q = Annotator.query
    if annotator_id is None:
        return q.all()
    else:
        annotator = q.filter(Annotator.id == annotator_id).first()
        annotator.expected_annotations = [complexity_level_for_comptext_pairs([a.pair])[0]
                                          for a in annotator.comptex_annotations]
        return annotator

def get_pair_info_full(pair_id):
    """
    Get information about a CompTextPair if pair_id is supplied, or all of
    them otherwise
    """
    q = CompTextPair.query.filter(CompTextPair.test != True)
    if pair_id is None:
        return q.all()
    else:
        pair = q.filter(CompTextPair.id == pair_id).first()
        pair.expected_annotation = complexity_level_for_comptext_pairs([pair])[0]
        return pair

def get_texts_not_in(pairs):
    """
    Find which texts are not present in the fully annotated pairs

    :return: List of CompTexts not present in the fully annotated pairs
    """
    count = {}
    for p in pairs:
        count[p.text1_id] = count.get(p.text1_id, 0) + 1
        count[p.text2_id] = count.get(p.text2_id, 0) + 1
    indexes = [i for i in range(1, CompText.query.count() + 1) if i not in count]
    if indexes:
        return CompText.query.filter(CompText.id.in_(indexes)).all()
    else:
        return None

def get_classifier_levels_for(pairs):
    """
    Get the complexity level given by the classifier for the specified
    CompTextPairs
    """
    return complexity_level_for_pairs([(p.text1.classifier_level, p.text2.classifier_level) for p in pairs])

def get_teacher_annotation_2combination(pairs):
    """
    Get a list of all the 2-combinations of non-test CompTextAnnotations for
    each CompTextPair

    :return: Iterator of 2-tuples with the combined Annotation complexity values
    """
    values = [get_complexities(p) for p in pairs]
    return chain.from_iterable((combinations(a, 2) for a in values))

def get_cohen_kappa(pairs):
    """
    Get the Cohen's Kappa statistic for the given pairs
    """
    combined_annotations = np.array(list(get_teacher_annotation_2combination(pairs)))
    return cohen_kappa_score(combined_annotations[:, 0], combined_annotations[:, 1])

