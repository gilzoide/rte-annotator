# -*- coding: utf-8 -*-

from flask_babel import lazy_gettext

from rteannotator import db

import re

DESIRED_ANNOTATIONS_PER_PAIR = 4
SIZE_INTERVALS = 2
WORD_COUNT_FACTOR = 1.3


class CompText(db.Model):
    '''
    A text that will form pairs for textual complexity annotation
    '''
    __tablename__ = "CompText"
    id = db.Column(db.Integer, primary_key=True)
    file_id = db.Column(db.Integer, db.ForeignKey('file.id'), index=True)

    text = db.Column(db.Text)
    title = db.Column(db.Text)
    complexity_level = db.Column(db.Integer)
    classifier_level = db.Column(db.Integer)
    word_count = db.Column(db.Integer)
    size_interval = db.Column(db.Integer)

    def __init__(self, xml_element, file_):
        self.file = file_
        self.text = xml_element.find('content').text
        self.complexity_level = int(xml_element.find('complexity-level').text)
        # find title
        m = re.search(ur"<title>(.+)</title>", self.text, flags=re.UNICODE)
        if m:
            self.title = m.group(1)
        # and set word count, which is used to find the size interval
        self.word_count = int(xml_element.find('word-count').text)

    def __repr__(self):
        return "<CompText {!r}; id: {}; complexity: {}>".format(
                        self.title or "", self.id, self.complexity_level)


class CompTextAnnotation(db.Model):
    '''
    Class for storing how a given annotator annotate a given TE pair.
    '''
    __tablename__ = "CompTextAnnotation"
    annotator_id = db.Column(db.Integer, db.ForeignKey('annotator.id'), primary_key=True)
    pair_id = db.Column(db.Integer, db.ForeignKey('CompTextPair.id'), primary_key=True)

    complexity = db.Column(db.Integer)
    comment = db.Column(db.Text)
    
    value_map = {1: lazy_gettext('Text 1 is much more complex than text 2'),
                 2: lazy_gettext('Text 1 is little more complex than text 2'),
                 3: lazy_gettext('Text 1 and text 2 have the same complexity'),
                 4: lazy_gettext('Text 2 is little more complex than text 1'),
                 5: lazy_gettext('Text 2 is much more complex than text 1')}

    def __init__(self, annotator, pair, complexity, comment):
        self.annotator = annotator
        self.pair = pair
        self.complexity = complexity
        self.comment = comment


class CompTextPair(db.Model):
    '''
    A pair of Texts, for complexity comparison
    '''
    __tablename__ = "CompTextPair"
    id = db.Column(db.Integer, primary_key=True)

    text1_id = db.Column(db.Integer, db.ForeignKey('CompText.id'))
    text2_id = db.Column(db.Integer, db.ForeignKey('CompText.id'))
    text_ids_unique = db.UniqueConstraint(text1_id, text2_id)
    test = db.Column(db.Boolean)

    text1 = db.relationship('CompText', foreign_keys=text1_id)
    text2 = db.relationship('CompText', foreign_keys=text2_id)

    # list of annotations
    annotations = db.relationship('CompTextAnnotation', backref='pair', lazy='dynamic',
                                  cascade='save-update, merge, delete, delete-orphan')

    def __init__(self, text1=None, text2=None, test=False):
        self.text1 = text1
        self.text2 = text2
        self.test = test

    def __repr__(self):
        return "<CompTextPair {}; {} x {}>".format(self.id, self.text1_id, self.text2_id)


class PendingCompTextAnnotation(db.Model):
    '''
    Class to store pairs already assigned/reserved to annotators
    '''
    __tablename__ = "PendingCompTextAnnotation"
    annotator_id = db.Column(db.Integer, db.ForeignKey('annotator.id'), primary_key=True)
    pair_id = db.Column(db.Integer, db.ForeignKey('CompTextPair.id'), primary_key=True)

    annotator = db.relationship('Annotator', backref='pending_comptext_annotations')
    pair = db.relationship('CompTextPair', backref=db.backref('pending_annotations',
                                                              cascade='all, delete-orphan'))

    def __init__(self, annotator, pair):
        self.annotator = annotator
        self.pair = pair

    def __repr__(self):
        return "<PendingCompTextAnnotation of {!r} by {!r}>".format(
                self.pair, self.annotator)


class TeacherInfo(db.Model):
    '''
    Teacher info for CompText annotators, in a separate table
    '''
    __tablename__ = "TeacherInfo"
    annotator_id = db.Column(db.Integer, db.ForeignKey('annotator.id'), primary_key=True)
    annotator = db.relationship('Annotator')

    email = db.Column(db.Text)
    full_name = db.Column(db.Text)
    school = db.Column(db.Enum("St Paul's", "Beit Yaacov", "Pentagono", "Poliedro", "EMEB", "14 bis", "diversas"))
    cicle = db.Column(db.Enum('Fund. I', 'Fund. II', 'todos', 'Ensino Medio', 'Fund. II e EM'))
    grade = db.Column(db.Text)

    def __init__(self, annotator, email=None, full_name=None, school=None, cicle=None,
                 grade=None):
        self.annotator = annotator
        self.email = email
        self.full_name = full_name
        self.school = school
        self.cicle = cicle
        self.grade = grade

