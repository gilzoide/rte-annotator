# -*- coding: utf-8 -*-

from flask import render_template, Blueprint, abort, request, jsonify, flash, url_for, redirect
from flask_breadcrumbs import register_breadcrumb
from flask_login import login_required, current_user
from flask_babel import gettext, lazy_gettext

from rteannotator import app, db
from rteannotator.utils import redirect_back
import model
import utils
import stats

from collections import Counter

bp = Blueprint('comptext', __name__, url_prefix='/comptext')

@bp.route('/')
@register_breadcrumb(bp, '.', 'Complexidade Textual')
@login_required
def index():
    return render_template('comptext-index.html')


@bp.route('/tutorial')
def tutorial():
    app.logger.warning("[User: {}] Entered tutorial".format(current_user.id))
    return render_template('comptext-tutorial.html',
                           difficulties=model.CompTextAnnotation.value_map.values(),
                           pair=utils.get_pair_for_tutorial())


@bp.route('/annotate-texts')
@login_required
def annotate_texts():
    pending = utils.get_pair_for_annotation()
    if pending is None:
        app.logger.warning("[User: {}] No more pairs for annotation".format(current_user.id))
        return render_template('no-more-comptexts.html')
    else:
        app.logger.warning("[User: {}] Pair for annotation: {}".format(current_user.id, pending.pair.id))
        return render_template('comptext-annotate.html',
                               difficulties=model.CompTextAnnotation.value_map.values(),
                               pair=pending.pair)

@bp.route('/submit-annotation', methods=['POST'])
@login_required
def submit_annotation():
    pair = request.values.get('pair')
    complexity = request.form.get('complexity')
    comment = request.form.get('comment')

    try:
        pair = int(pair)
        complexity = int(complexity)

        utils.annotate_pair(pair, complexity, comment)
        flash(gettext('Pair annotated successfully') + '!', 'info')
        flash(gettext('Annotate new pair') + '.', 'info')
        app.logger.warning("[User: {}] Annotated pair {} with value {}".format(
                           current_user.id, pair, complexity))
    except TypeError:
        flash(gettext('Complexity annotation required'), 'error')
    except AssertionError as ex:
        app.logger.error(str(ex))
        abort(401)
    return redirect_back('index')


@bp.route('/view-texts/', defaults={'page': 0, 'text_id': None})
@bp.route('/view-texts/<int:page>', defaults={'text_id': None})
@bp.route('/view-texts/<int:page>/<int:text_id>')
@register_breadcrumb(bp, '.texts', 'Textos')
@login_required
def view_texts(page, text_id):
    if current_user.have_special_permissions():
        if text_id is not None:
            text = utils.get_text(text_id)
            if text is not None:
                return render_template('view-a-comptext.html',
                                       page=page,
                                       text=text)
            else:
                return abort(404)
        else:
            return render_template('view-comptexts.html',
                                   page=page,
                                   texts=utils.get_text_batch(page))
    else:
        return abort(404)


@bp.route('/pair-texts', methods=['GET'])
@login_required
def pair_texts():
    if current_user.have_special_permissions():
        texts = model.CompText.query.all()
        values = request.values
        def cmplx2str(p):
            '''Stringify a Pair texts' complexity'''
            complexities = sorted([p[0].complexity_level, p[1].complexity_level])
            return "{}x{}".format(*complexities)
        if values.get('tamanho'):
            pairs = utils.pair_texts_by_size(texts, int(values.get('tamanho')), True)
            count = Counter(map(cmplx2str, pairs))
        elif values.get('fator'):
            pairs, _ = utils.pair_texts_by_word_count(texts, float(values.get('fator')), True)
            pairs = list(pairs)
            count = Counter(map(cmplx2str, pairs))
        else:
            pairs = None
            count = None
        return render_template('pair-comptexts.html',
                               pairs=pairs,
                               count=count)
    else:
        return abort(404)

@bp.route('/view-annotator/', defaults={"annotator_id": None}, methods=['GET'])
@bp.route('/view-annotator/<int:annotator_id>', methods=['GET'])
@register_breadcrumb(bp, '.annotators', lazy_gettext('Annotators'))
@login_required
def view_annotator(annotator_id):
    if current_user.have_special_permissions():
        annotator = utils.get_annotator_info_full(annotator_id)
        if annotator is None:
            abort(404)
        return render_template('comptext-view-annotator.html',
                               annotator=annotator,
                               only_one=bool(annotator_id))
    else:
        abort(404)

@bp.route('/view-pair/', defaults={"pair_id": None}, methods=['GET'])
@bp.route('/view-pair/<int:pair_id>', methods=['GET'])
@register_breadcrumb(bp, '.pairs', lazy_gettext('Pairs'))
@login_required
def view_pair(pair_id):
    if current_user.have_special_permissions():
        pair = utils.get_pair_info_full(pair_id)
        if pair is None:
            abort(404)
        return render_template('comptext-view-pair.html',
                               pair=pair,
                               difficulty_labels=model.CompTextAnnotation.value_map,
                               only_one=bool(pair_id))
    else:
        abort(404)

@bp.route('/stats')
@login_required
def stats():
    '''
    Route for Statistics tests
    '''
    if current_user.have_special_permissions():
        num_annotations = utils.get_num_annotations()
        all_pairs = utils.get_non_test_pairs()
        fully_annotated = utils.get_fully_annotated_pairs()
        texts_not_in_fully_annotated = utils.get_texts_not_in(fully_annotated)
        expected_num_annotated_pairs = utils.get_non_test_pair_count()
        expected_num_annotations = expected_num_annotated_pairs * model.DESIRED_ANNOTATIONS_PER_PAIR
        
        for p in fully_annotated:
            p.scores = utils.get_complexities(p)
        kappa4all, kappa4teachers = utils.kappa_for_pairs(fully_annotated)
        cohen = utils.get_cohen_kappa(all_pairs)

        confusion = utils.confusion_for_majoritary_voted()
        num_majoritary_voted = confusion[-1,-1]
        normalized_confusion = confusion / (num_majoritary_voted / 100)

        actual_complexities = utils.complexity_level_for_comptext_pairs(fully_annotated)
        icc = utils.get_icc(fully_annotated, actual_complexities)
        icc_aggregated = utils.get_icc(fully_annotated, actual_complexities, aggregate_12_45=True)
        icc4all = utils.get_icc_with_na(all_pairs)

        icc_classifier = utils.get_icc(fully_annotated, utils.get_classifier_levels_for(fully_annotated))
        return render_template('comptext-analytics.html',
                               fully_annotated=fully_annotated,
                               texts_not_in_fully_annotated=texts_not_in_fully_annotated,
                               kappa4all=kappa4all,
                               kappa4teachers=kappa4teachers,
                               cohen=cohen,
                               real_name=request.values.get("real_name") != "false",
                               value_labels=model.CompTextAnnotation.value_map.values(),
                               num_annotations=num_annotations,
                               expected_num_annotated_pairs=expected_num_annotated_pairs,
                               expected_num_annotations=expected_num_annotations,
                               confusion_matrix=confusion,
                               num_majoritary_voted=int(num_majoritary_voted),
                               normalized_confusion=normalized_confusion,
                               icc=icc,
                               icc_aggregated=icc_aggregated,
                               icc_classifier=icc_classifier,
                               icc4all=icc4all,
                               DESIRED_ANNOTATIONS_PER_PAIR=model.DESIRED_ANNOTATIONS_PER_PAIR)
    else:
        return abort(404)

@bp.route('/test-stats')
@login_required
def test_stats():
    if current_user.have_special_permissions():
        all_pairs = utils.get_non_test_pairs()
        icc = utils.get_icc_with_na(all_pairs)
        return jsonify({"icc": icc})
    else:
        return abort(404)

