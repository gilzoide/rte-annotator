# -*- coding: utf-8 -*-

'''
Functions dealing with the XML CompText files.
'''

from model import CompText, WORD_COUNT_FACTOR
from utils import pair_texts_by_word_count
from rteannotator import db

def process_comptext_file(file_, root):
    '''
    Process a CompText XML tree, adding all texts to the DB
    '''
    all_texts = []
    for xml_text in root:
        text = CompText(xml_text, file_)
        db.session.add(text)
        all_texts.append(text)

    # sort by word count
    def get_count(t):
        return t.word_count
    all_texts.sort(key=get_count)

    pairs4annotation, pairs4test = pair_texts_by_word_count(all_texts, WORD_COUNT_FACTOR)
    db.session.add_all(pairs4annotation)
    db.session.add_all(pairs4test)
    db.session.commit()
