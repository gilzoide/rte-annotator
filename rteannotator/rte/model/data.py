# -*- coding: utf-8 -*-

'''
This module contains classes for the entities representing in the
database.
'''

from rteannotator import db
from flask_login import current_user

DESIRED_ANNOTATIONS_PER_PAIR = 4

class Pair(db.Model):
    '''
    A TE candidate pair and its associated metadata.
    '''
    id = db.Column(db.Integer, primary_key=True)
    id_in_file = db.Column(db.Integer, index=True)
    t = db.Column(db.Text)
    h = db.Column(db.Text)

    similarity = db.Column(db.Float)
    alpha1 = db.Column(db.Float)
    alpha2 = db.Column(db.Float)
    cluster = db.Column(db.String(50))

    file_id = db.Column(db.Integer, db.ForeignKey('file.id'), index=True)
    flag_delete = db.Column(db.Boolean, default=False)
    
    # signals if the pair has already been reviewed as appropriate for the corpus
    reviewed = db.Column(db.Boolean, default=False)
    
    # indicate who reviewed the pair
    reviewer_id = db.Column(db.Integer, db.ForeignKey('annotator.id'), index=True)
    
    # reviewed pairs are pairs checked for validity (i.e., edited if necessary, 
    # only kept if not noisy) but not actually annotated
    reviewer = db.relationship('Annotator', 
                               backref=db.backref('reviewed_pairs', lazy='dynamic'))
    
    # list of annotations objects: each one was an annotation instance
    # by a different user
    annotations = db.relationship('RTEAnnotation', backref='pair', lazy='dynamic',
                                  cascade='save-update, merge, delete, delete-orphan')
        
    def __repr__(self):
        return '<Pair {} in file {}>'.format(self.id, self.file_id)
    
    def __init__(self, xml_element=None, file_=None):
        '''
        :param xml_element: a Pair XML element from a file in the
            RTE challenge format. It should contain the t and h
            subelements, plus the attributes id, entailment and
            similarity.
        '''
        if xml_element is None:
            return
        self.t = xml_element.find('t').text
        self.h = xml_element.find('h').text
        self.attributes = xml_element.attrib   
        self.id_in_file = xml_element.get('id')
        self.file = file_
        for item in ['alpha1', 'alpha2', 'similarity', 'cluster']:
            setattr(self, item, xml_element.get(item))
    
class RTEAnnotation(db.Model):
    '''
    Class for storing how a given annotator annotate a given TE pair.
    '''
    annotator_id = db.Column(db.Integer, db.ForeignKey('annotator.id'), primary_key=True)
    pair_id = db.Column(db.Integer, db.ForeignKey('pair.id'), primary_key=True)

    # RTE specific
    entailment = db.Column(db.Integer, index=True)
    similarity = db.Column(db.Integer, index=True)
    
    value_map = {1: 'First entails second',
                 2: 'Second entails first',
                 3: 'Paraphrase',
                 4: 'None'}

class PendingAnnotation(db.Model):
    '''
    Class to store which pairs are assigned to which annotators to annotate.
    '''
    annotator_id = db.Column(db.Integer, db.ForeignKey('annotator.id'), primary_key=True)
    pair_id = db.Column(db.Integer, db.ForeignKey('pair.id'), primary_key=True)
    annotator = db.relationship('Annotator', backref='pending_annotations')
    pair = db.relationship('Pair', 
                           backref=db.backref('pending_annotations', 
                                              lazy='dynamic',
                                              cascade='all, delete-orphan'))
    
    def __repr__(self):
        return '<Pending Annotation by annotator %s on pair %s>' % (self.annotator_id,
                                                                    self.pair_id)    

class PendingReview(db.Model):
    '''
    Class to store which pairs are being evaluated (for deciding if it should be edited or
    discarded) by which users.
    '''
    annotator_id = db.Column(db.Integer, db.ForeignKey('annotator.id'))
    pair_id = db.Column(db.Integer, db.ForeignKey('pair.id'), primary_key=True)
    annotator = db.relationship('Annotator', backref='pending_review')
    pair = db.relationship('Pair', 
                           backref=db.backref('pending_review', 
                                              cascade='all, delete-orphan'))
    
    def __repr__(self):
        return '<Pending Review by annotator %s on pair %s>' % (self.annotator_id,
                                                                self.pair_id)

class ReviewCounter(db.Model):
    '''
    Count how many pairs an annotator reviewed in a file.
    
    This table is necessary instead of using a field for annotator in the pairs because
    deleted pairs should also be counted.
    '''
    annotator_id = db.Column(db.Integer, db.ForeignKey('annotator.id'), primary_key=True)
    file_id = db.Column(db.Integer, db.ForeignKey('file.id'), primary_key=True)
    count = db.Column(db.Integer)
    
    annotator = db.relationship('Annotator', 
                                backref=db.backref('review_counts', lazy='dynamic'))
    file = db.relationship('File', 
                           backref=db.backref('review_counts', lazy='dynamic'))
    
    def __repr__(self):
        return '<ReviewCounter for user %d on file %d: %d>' % (self.annotator_id,
                                                               self.file_id,
                                                               self.count)
    
