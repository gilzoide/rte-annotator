# -*- coding: utf-8 -*-

'''
This module contains functions operating over the database models.
'''

from collections import Counter
from sqlalchemy import and_, func
from cStringIO import StringIO
from flask_babel import gettext
 
from rteannotator import db, login_manager
import data
from rteannotator.rte import xmlutils
from rteannotator.model import Annotator, File
from data import RTEAnnotation as Annotation, Pair, PendingReview, ReviewCounter, PendingAnnotation
from sqlalchemy.orm.exc import NoResultFound
import flask_scrypt as scrypt
from flask_login import current_user

def create_user_if_new(username):
    '''
    Check whether exists an annotator user with the given username.
    If not, create one.
    '''
    result = Annotator.query.filter_by(name=username)
    if result.count() == 0:
        annotator = Annotator(name=username)
        db.session.add(annotator)
        db.session.commit()
        return annotator
    
    return result.first()

def create_user(username, password):
    '''
    Create a user with the given name and password. A hashed version of the
    password is saved to the DB.
    '''
    salt = scrypt.generate_random_salt()
    hashed_pw = scrypt.generate_password_hash(password, salt)
    user = Annotator(name=username, salt=salt, password=hashed_pw)
    db.session.add(user)
    db.session.commit()
    return user

@login_manager.user_loader
def load_user(user_id):
    return Annotator.query.get(int(user_id))

def change_user_password(old_password, new_password):
    '''
    Change the current user's password
    '''
    if isinstance(old_password, unicode):
        old_password = old_password.encode('utf-8')
    
    if isinstance(new_password, unicode):
        new_password = new_password.encode('utf-8')
    
    if current_user.password is None or scrypt.check_password_hash(old_password, 
                                                                   current_user.password.encode("utf-8"), 
                                                                   current_user.salt):
        if current_user.salt is None:
            current_user.salt = scrypt.generate_random_salt()
        
        new_hashed = scrypt.generate_password_hash(new_password, current_user.salt)
        current_user.password = new_hashed
        db.session.commit()
        return True
    else:
        # invalid password
        return False


def get_pair_entailment_count(pair_id_query):
    '''
    Return a query for Pair id, entailment type and annotation count.
    It requires a query for the pair ids to be considered.
    '''
    Pair = data.Pair
    q = db.session.query(Pair.id, Annotation.entailment, func.count(Annotation.entailment))
    q = q.join(Annotation).filter(Pair.id.in_(pair_id_query)).group_by(Pair.id, Annotation.entailment)
    return q

def generate_annotated_file(file_id, num_pairs, agreeing_annotations=0):
    '''
    Fetch pairs from the database in order to create an RTE/STS file with the specified
    number of pairs.
    
    :param agreeing_annotations: minimal number of agreeing entailment annotations
        a pair must have. If 0, defaults to half + 1.
    '''
    q = get_annotated_pairs_query(file_id)
    qid = q.with_entities(Pair.id)
    count_query = get_pair_entailment_count(qid)
    
    if agreeing_annotations == 0:
        agreeing_annotations = data.DESIRED_ANNOTATIONS_PER_PAIR / 2 + 1
        
    # filter count_query so that it only contains the pairs with the requested number
    # of equal entailment annotations
    count_query = count_query.having(func.count(Annotation.entailment) >= agreeing_annotations)
    count_query = count_query.with_entities(Pair.id, Annotation.entailment)
        
    entailment_per_pair = count_query.limit(num_pairs).all()
    entailment_per_pair = dict(entailment_per_pair)
    
    # now, take these id's
    # we want the similarity average for all of them, including similarity annotations
    # whose entailment counterpart was ignored
    qid = count_query.with_entities(Pair.id)
    
    #TODO: more efficient query
    pairs = Pair.query.filter(Pair.id.in_(qid)).limit(num_pairs).all()
    
    q_similarity = db.session.query(Annotation.pair_id, func.avg(Annotation.similarity))
    q_similarity = q_similarity.filter(Annotation.pair_id.in_(qid)).group_by(Annotation.pair_id)
    similarity_per_pair = q_similarity.limit(num_pairs).all()
    similarity_per_pair = dict(similarity_per_pair)
    
    xml_string = xmlutils.create_rte_file(pairs, entailment_per_pair, 
                                          similarity_per_pair, True)
    return StringIO(xml_string)

def add_annotation_counts(users):
    '''
    Add the number of annotations that each user did to a list
    of users.
    '''
    counts = count_annotations_per_user()
    for i, user in enumerate(users):
        if user.id in counts:
            user.annotation_count = counts[user.id]
        else:
            user.annotation_count = 0
        
        users[i] = user

def count_annotations_per_user():
    '''
    Count the number of annotations for each user
    '''
    q = db.session.query(Annotator.id, func.count(Annotation.pair_id))
    q = q.join(Annotation).join(Pair).filter(Pair.flag_delete == False)
    q = q.group_by(Annotator.id)
    
    data = q.all()
    counts = dict(data)
    
    return counts

def generate_rte_file(file_id):
    '''
    Generate a temporary XML file containing the reviewed data from the file with
    the given file_id. Pairs not reviewed or flagged for deletion are not included. 
    
    Currently, annotations are ignored.
    '''
    #TODO: include annotations
    pairs = Pair.query.filter(Pair.file_id==file_id,
                              Pair.reviewed==True,
                              Pair.flag_delete==False).all()
                              
    xml_string = xmlutils.create_rte_file(pairs)
    return StringIO(xml_string)

def validate_login(username, password):
    '''
    Validate the user login using proper password encryption
    '''
    try:
        user = find_annotator(username)
    except Exception as e:
        print e
        return (False, 'Invalid username')
    
    if user.password is None:
        return (True, '')
    
    if scrypt.check_password_hash(password.encode('utf-8'), user.password.encode('utf-8'), user.salt):
        return (True, '')
    else:
        return (False, gettext('Wrong password'))

def get_annotators(annotator_ids):
    '''
    Return a list of annotators with the supplied id's.
    '''
    q = Annotator.query.filter(Annotator.id.in_(annotator_ids))
    annotators = q.order_by(Annotator.id).all()
    return annotators

def get_unreviewed_pair(annotator_name, file_id):
    '''
    Get a pair from the given file that was not yet reviewed. The pair is assigned
    to the given user.
    '''
    annotator = find_annotator(annotator_name)
    Pair = data.Pair
    
    # first, check if there was some pending pair for the annotator
    pending_query = PendingReview.query.filter_by(annotator_id=annotator.id)
    pending_query = pending_query.join(Pair).filter(Pair.file_id==file_id)
    if pending_query.count() > 0:
        pending = pending_query.one()
        return pending.pair
    
    # pick a pair that has not been evaluated yet AND is not allocated to another user
    pair_query = Pair.query.filter(Pair.reviewed==False, Pair.pending_review==None,
                                   Pair.file_id==file_id)
    
    pair = pair_query.first()
    if pair is None:
        return None
    
    review = PendingReview(annotator_id=annotator.id,
                           pair_id=pair.id) 
    db.session.add(review)
    db.session.commit()
    
    return pair

def add_to_review_counter(annotator_id, file_id):
    '''
    Add one to the review counter of the given annotator on the given file.
    '''
    q = get_review_by_user_query(annotator_id, file_id)
    review_counter = q.first()
    if review_counter is None:
        review_counter = ReviewCounter(annotator_id=annotator_id, 
                                       file_id=file_id,
                                       count=1)
        db.session.add(review_counter)
    else:
        review_counter.count += 1

def mark_as_reviewed(annotator_name, pair_id):
    '''
    Mark a given pair as having been reviewed. Also, remove the pending review.
    '''
    pair = Pair.query.filter_by(id=pair_id).one()
    annotator = find_annotator(annotator_name)
    
    pair.reviewed = True
    pair.reviewer = annotator
    delete_pending_review(pair_id)
    add_to_review_counter(annotator.id, pair.file_id)
    
    db.session.commit()

def change_pair(pair_id, new_t, new_h, update_reviews=True):
    '''
    Perform a change in a pair. It is updated with new T and H components.
    
    If update reviews is True, update the review count for the given user and
    log the pair as being reviewed by him/her. If False, don't log or count
    anything.
    '''
    pair = Pair.query.filter_by(id=pair_id).one()
    
    pair.t = new_t
    pair.h = new_h
    pair.reviewed = True
    pair.reviewer = current_user
    if update_reviews:
        delete_pending_review(pair_id)
        add_to_review_counter(current_user.id, pair.file_id)
    
    db.session.commit()

def delete_annotations(pair_id):
    '''
    Delete all annotations on the given pair.
    '''
    q = Annotation.query.filter(Annotation.pair_id == pair_id)
    q.delete(synchronize_session=False)
    
    for item in q.all():
        print item
    
    db.session.commit()

def delete_pair(annotator_name, pair_id):
    '''
    Delete a pair from the DB and remove the pending review from the user.
    '''
    annotator = find_annotator(annotator_name)
    pair = Pair.query.filter_by(id=pair_id).one()
    
    db.session.delete(pair)
    delete_pending_review(pair_id)
    add_to_review_counter(annotator.id, pair.file_id)
    
    db.session.commit()

def delete_pending_review(pair_id):
    '''
    Remove from the DB a pending review by the current user.
    '''
    try:
        review = PendingReview.query.filter_by(annotator_id=current_user.id,
                                               pair_id=pair_id).one()
        db.session.delete(review)
    except NoResultFound:
        # no review (or it was deleted by cascade)
        pass                                           
    

def annotate_pair_rte(annotation_type, pair_id, annotator_id, value):
    '''
    Annotate a pair with the given value.
    If the annotator had previously annotated that pair, update the existing
    annotation.
    :param annotation_type: 'similarity' or 'entailment'
    '''
    result = Annotation.query.filter_by(annotator_id=annotator_id, 
                                        pair_id=pair_id)
    
    assert annotation_type in ['similarity', 'entailment'], \
        'Invalid annotation type'
    
    if result.count() != 0:
        # user had previously annotated this pair
        annotation = result.one()
    else:
        # new annotation
        annotation = Annotation(annotator_id=annotator_id,
                                pair_id=pair_id)
        db.session.add(annotation)
    
    setattr(annotation, annotation_type, value)
    
    # if both kinds of annotation are done, delete from the pending ones
    if annotation.entailment is not None and annotation.similarity is not None:
        q = PendingAnnotation.query.filter(PendingAnnotation.annotator_id == annotator_id,
                                                 PendingAnnotation.pair_id == pair_id)
        pending = q.first()
        if pending is not None:
            db.session.delete(pending)
    
    db.session.commit()

def toggle_show_all_pairs(username, switch):
    '''
    Set the value of the variable controlling whether the user sees all
    pairs (switch == True) or only unannotated ones (False).
    
    Commit to the DB afterwards.
    '''
    annotator_id = find_annotator(username).id
    annotator = Annotator.query.filter_by(id=annotator_id).first()
    annotator.show_all_pairs = switch
    db.session.commit()
    

def find_file_id(filename):
    '''
    Return the id for the given file searching by its name.
    '''
    return File.query.filter_by(name=filename).one().id

def find_file(filename):
    return File.query.filter_by(name=filename).one()

def find_annotator(annotator_name):
    '''
    Return the id for the annotator with the given name.
    '''
    return Annotator.query.filter_by(name=annotator_name).one()

def get_file_annotation_query(file_id_or_name):
    '''
    Return a query object for all annotations of pairs in the given file.
    Only retrieve pairs ready to be used.
    '''
    if isinstance(file_id_or_name, int):
        file_id = file_id_or_name
    else:
        file_id = find_file_id(file_id_or_name)
    
    # filter the given file
    q = Annotation.query.join(Pair).filter(Pair.file_id==file_id,
                                                Pair.reviewed==True,
                                                Pair.flag_delete==False)
        
    return q

def get_pairs_file_query(file_id_or_name, reviewed=False):
    '''
    Return a query object for the pairs in the file with the given name.
    If cleaned is True, only return cleaned pairs.
    '''
    if isinstance(file_id_or_name, str):
        file_id = find_file_id(file_id_or_name)
    else:
        file_id = file_id_or_name
    
    q = Pair.query.filter(Pair.file_id==file_id)
    if reviewed:
        q = q.filter(Pair.reviewed==True, Pair.flag_delete==False)
    
    return q

def count_annotated_pairs(pair_query):
    '''
    Count how many pairs (in the given query) have been annotated and are not flagged
    for deletion/edition.
    '''
    q = pair_query.filter(Pair.annotations != None,
                          Pair.reviewed == True,
                          Pair.flag_delete == False)

    return q.count()

def count_annotations_by_user(pair_query):
    '''
    Count how many annotations by the current user in the given query
    '''
    q = pair_query.join(Annotation).filter(Annotation.annotator_id == current_user.id)
    
    return q.count()

def count_annotated_enough(annotations):
    '''
    Count how many pairs are annotated by a sufficient number of annotations. 
    '''
    c = Counter(a.pair_id for a in annotations 
                if a.similarity is not None and a.entailment is not None)
    
    return len([count for count in c.values() if count == data.DESIRED_ANNOTATIONS_PER_PAIR])

def get_annotation_counters(filename):
    '''
    Return an entailment annotation counter and a similarity one.
    '''
    file_id = find_file_id(filename)
    q = Annotation.query.join(Pair)
    q = q.filter(Pair.file_id == file_id,
                 Pair.reviewed == True,
                 Pair.flag_delete == False)
    annotations = q.all()
    entailment_counter = Counter()
    similarity_counter = Counter()
    
    for a in annotations:
        similarity_counter[a.similarity] += 1
        try:
            entailment_label = Annotation.rte_value_map[a.entailment]
            entailment_counter[entailment_label] += 1
        except KeyError:
            pass
    
    return (entailment_counter, similarity_counter)

def get_annotated_pairs_query(file_name_or_id, columns=None):
    '''
    Return the query for all valid pairs (reviewed, not deleted) 
    with enough annotations from the given file. It uses group by pair.
    
    :param file_name_or_id: filename, file id or None. If None, pairs from
        all files are retrieved.
    '''
    if isinstance(file_name_or_id, basestring):
        file_id = find_file_id(file_name_or_id)
    else:
        file_id = file_name_or_id
    
    q = Pair.query.join(Annotation)
    q = q.filter(Annotation.similarity != None,
                 Annotation.entailment != None,
                 Pair.reviewed == True,
                 Pair.flag_delete == False)
    
    if file_id is None:
        q = q.join(File).filter(File.available == True)
    else:
        q = q.filter(Pair.file_id == file_id)
        
    q = q.group_by(Pair.id).having(func.count(Annotation.annotator_id) == data.DESIRED_ANNOTATIONS_PER_PAIR)
    
    if columns is not None:
        q = q.with_entities(*columns)
    
    return q

def get_all_valid_annotations():
    '''
    Return all annotations on valid files, ordered by pair id.
    '''
    q_annotation = db.session.query(Annotation.annotator_id, Annotation.pair_id,
                                    Annotation.entailment, Annotation.similarity)
    q_annotation = q_annotation.join(Pair).join(File)
    q_annotation = q_annotation.filter(Annotation.similarity != None,
                                       Annotation.entailment != None,
                                       Pair.reviewed == True,
                                       Pair.flag_delete == False,
                                       File.available == True)
    
    return q_annotation.order_by(Annotation.pair_id).all()

def get_annotated_pairs(filename):
    '''
    Return all valid pairs (reviewed, not deleted) with enough annotations
    from the given file. 
    '''
    q = get_annotated_pairs_query(filename)
    
    return q.all()

def count_reviewed_and_kept(file_id):
    '''
    Count how many pairs in the given file were reviewed and kept 
    (and also not flagged for deletion).
    '''
    q = Pair.query.filter(and_(Pair.file_id==file_id,
                               Pair.reviewed==True,
                               Pair.flag_delete==False))
    
    return q.count()

def get_review_by_user_query(annotator_id, file_id=None):
    '''
    Return a query to ReviewCounter by user and optionally file.
    '''
    if file_id is not None:
        condition = and_(ReviewCounter.file_id==file_id,
                         ReviewCounter.annotator_id==annotator_id)
    else:
        condition = ReviewCounter.file_id==file_id
    
    q = ReviewCounter.query.filter(condition)
    return q

def count_reviewed_by_user(file_id):
    '''
    Count pairs annotated in the file by the given user, kept or not.
    '''
    annotator_id = current_user.id
    q = get_review_by_user_query(annotator_id, file_id)
    
    review_counter = q.first()
    if review_counter is None:
        return 0
    else:
        return review_counter.count

def filter_annotations_by_user(annotation_query):
    '''
    Filter the query to contain only pairs annotated by a given user.
    They should contain entailment and similarity.
    '''
    condition = and_(Annotation.annotator_id == current_user.id,
                     Annotation.entailment != None,
                     Annotation.similarity != None)
    q = annotation_query.filter(condition)
    return q

def flag_pair(pair_id, operation):
    '''
    Flag a pair for either editing or deleting. Flagging for editing means the same
    as tagging it as not reviewed.
    '''
    pair = Pair.query.get(pair_id)
    if operation == 'edit':
        pair.reviewed = False
        pair.reviewer = None
        
        # delete other annotations on this pair to keep consistency
        q = Annotation.query.filter(Annotation.pair_id == pair_id)
        q.delete(synchronize_session=False)
        
    elif operation == 'delete':
        pair.flag_delete = True
        # remove from any annotator's assigned queue
        q = PendingAnnotation.query.filter_by(pair_id=pair_id) 
        q.delete(synchronize_session=False)
    else:
        return False
    
    db.session.commit()
    return True

def filter_pairs_ready(pair_query):
    '''
    Add a filter to the query to discard pairs that were annotated
    by a sufficient number of annotators other than the annotator with 
    the given id. That is, it allows the annotator to see pairs
    he/she annotated.
    '''
    # get the pairs with sufficient annotations and use them in an except clause
    condition = and_(Annotation.entailment != None,
                     Annotation.similarity != None,
                     Annotation.annotator_id != current_user.id)
    q = pair_query.join(Annotation).filter(condition)
    q = q.group_by(Pair).having(func.count(Annotation.annotator_id) >= data.DESIRED_ANNOTATIONS_PER_PAIR)
    
    return pair_query.except_(q)

def get_pairs_for_annotation(file_id, number):
    '''
    Return a given number of pairs to be annotated by the current user.
    
    Returned pairs are already reviewed, not flagged for deletion, have not been
    annotated by nor is assigned to the desired amount of annotators. They are selected
    randomly.
    '''
    q_assigned = PendingAnnotation.query.join(Pair)
    q_assigned = q_assigned.filter(PendingAnnotation.annotator_id == current_user.id,
                                   Pair.file_id == file_id)
    result = q_assigned.limit(number).all()
    assigned_pairs = [item.pair for item in result]
    if len(assigned_pairs) == number:
        return assigned_pairs
    
    q = Pair.query.filter(Pair.reviewed == True,
                          Pair.flag_delete == False,
                          Pair.file_id == file_id)
    
    # fetch pairs annotated / assigned enough times
    q_exclude = Pair.query.outerjoin(Annotation).outerjoin(PendingAnnotation)
    annotators_sum = func.count(Annotation.annotator_id) + func.count(PendingAnnotation.annotator_id)
    q_exclude = q_exclude.group_by(Pair).having(annotators_sum >= data.DESIRED_ANNOTATIONS_PER_PAIR)
    q_exclude = q_exclude.with_entities(Pair.id)
    
    # fetch pairs already annotated by the current annotator
    q_annotated = db.session.query(Annotation.pair_id).filter(Annotation.annotator_id == current_user.id)
    q_assigned = q_assigned.with_entities(PendingAnnotation.pair_id)
    q_exclude = q_exclude.union(q_annotated).union(q_assigned)
    
    q = q.filter(~Pair.id.in_(q_exclude))
    
    # order by random is reportedly slow, but I **suppose** it is the fastest alternative
    # if we fetch several rows at once
    # we could assign more pairs to reduce queries, but this has the bad effect of locking
    # some pairs to slower annotators
    q = q.order_by(func.random()).limit(number)
    
    for pair in q.all():
        pending = PendingAnnotation(annotator_id=current_user.id, pair_id=pair.id)
        db.session.add(pending)
        
        if len(assigned_pairs) < number:
            assigned_pairs.append(pair)
    
    db.session.commit()
    
    return assigned_pairs

def filter_pairs_annotated_by_user(pair_query):
    '''
    Add a filter to the query such that only pairs not annotated by the given 
    annotator are kept.
    :param pair_query: a query for all pairs we are interested in
    '''
    pair_query.outerjoin(Annotation)
    
    # this query contains the annotated pairs (which will be ignored)
    # only consider annotations of BOTH entailment and similarity
    annotated_pairs = Pair.query.join(Annotation)
    annotated_pairs = annotated_pairs.filter(Annotation.annotator_id == current_user.id,
                                             Annotation.entailment != None,
                                             Annotation.similarity != None)
    
    return pair_query.except_(annotated_pairs) 

def add_annotations_by_user(pairs):
    '''
    Filter the given pairs such that they only contain annotations made by the
    current user.
    '''    
    for i, pair in enumerate(pairs):
        annotations_by_user = [ann for ann in pair.annotations
                               if ann.annotator_id == current_user.id]
        
        msg = 'More than one annotation from user to pair %d' % pair.id
        assert len(annotations_by_user) <= 1, msg
        if len(annotations_by_user) == 1:
            annotation = annotations_by_user[0]
        else:
            annotation = None
        
        pairs[i].annotation = annotation
    
