# -*- coding: utf-8 -*-

import os
import json
import flask
import werkzeug
from flask import session, request, Blueprint
from flask_login import login_required, login_user, logout_user, current_user
from flask_babel import gettext
from flask_breadcrumbs import register_breadcrumb
from sqlalchemy import update

from model import *
from rteannotator import model
from rteannotator import utils
from rteannotator import app
from rteannotator import db
from rteannotator import babel
from config import LANGUAGES

bp = Blueprint('rte', __name__, url_prefix='/rte')

ITEMS_PER_PAGE = 5

@bp.route('/')
@register_breadcrumb(bp, '.', 'RTE')
@login_required
def index():
    return flask.render_template('rte-index.html')

@bp.route('/choose-file', methods=['GET', 'POST'])
@login_required
def choose_file():
    filename = request.form['filename']
    if not utils.check_filename(filename):
        return flask.render_template('forbidden.html')

    session['filename'] = filename
    return flask.redirect(flask.url_for('display_rte_data'))

@bp.route('/review-file/', defaults={'page': None, 'file_id': None})
@bp.route('/review-file/<int:file_id>/', defaults={'page': 1})
@bp.route('/review-file/<int:file_id>/<int:page>')
@login_required
def review_file(file_id, page=1):
    '''
    Display previously annotated pairs.
    '''
    if file_id is None:
        return ''

    rte_file = model.File.query.get(file_id)
    rte_file.load_metadata()
    pair_query = get_pairs_file_query(file_id, True)
    pair_annotated_by_user_query = filter_annotations_by_user(pair_query)
    num_pairs = pair_annotated_by_user_query.count()
    paginated_query = utils.paginate_query(pair_query, page, ITEMS_PER_PAGE)
    num_pages = utils.find_num_pages(num_pairs, ITEMS_PER_PAGE)

    pairs = paginated_query.all()
    add_annotations_by_user(pairs)
    return flask.render_template('view-rte.html',
                                 page=page,
                                 file=rte_file,
                                 pairs=pairs,
                                 num_pages=num_pages,
                                 annotations_per_pair=DESIRED_ANNOTATIONS_PER_PAIR,
                                 showing_all=current_user.show_all_pairs)

@bp.route('/annotate-file/', defaults={'page': None, 'file_id': None})
@bp.route('/annotate-file/<int:file_id>', defaults={'page': 1})
@bp.route('/annotate-file/<int:file_id>/<int:page>')
@login_required
def annotate_file(file_id, page):
    '''
    Display an RTE file for annotation.
    '''
    rte_file = model.File.query.get(file_id)
    rte_file.load_metadata()

    # get ITEMS_PER_PAGE pairs for this annotator
    pairs = get_pairs_for_annotation(file_id, ITEMS_PER_PAGE)

    add_annotations_by_user(pairs)

    return flask.render_template('view-rte.html',
                                 page=page,
                                 file=rte_file,
                                 pairs=pairs,
                                 annotations_per_pair=DESIRED_ANNOTATIONS_PER_PAIR,
                                 showing_all=current_user.show_all_pairs)

@bp.route('/examples')
def examples():
    '''
    Show annotation examples
    '''
    return flask.render_template('examples.html')

# @bp.route('/combine-files', methods=['GET', 'POST'])
# @login_required
# def combine_files():
#     '''
#     Combine the annotated data in the given files, providing a single
#     one to download.
#     '''
#     if request.method == 'GET':
#         return flask.render_template('choose-file.html',
#                                      filenames=utils.get_uploaded_files(),
#                                      action_url=flask.url_for('combine_files'),
#                                      multiselect=True)
#
#     filenames = request.form.getlist('filenames[]')
#     all_pairs = []
#     for filename in filenames:
#         if not utils.check_filename(filename):
#             return flask.render_template('forbidden.html')
#
#         full_path = os.path.join(utils.get_upload_folder(), filename)
#         file_data = utils.load_pairs_from_xml(full_path)
#         pairs = file_data.pairs
#         annotated_pairs = [pairs[num] for num in pairs
#                            if pairs[num].entailment != 'UNKNOWN']
#         all_pairs.extend(annotated_pairs)
#
# #     file_data = model.FileData(None, all_pairs, len(all_pairs))
#
#     tree = file_data.generate_xml()
#     combined_file = 'combined-rte.xml'
#     path = os.path.join(utils.get_upload_folder(), combined_file)
#     tree.write(path, 'utf-8', True)
#
#     return flask.send_file(path, as_attachment=True,
#                            mimetype='text/plain')


@bp.route('/statistics/', defaults={'file_id': None})
@bp.route('/statistics/<int:file_id>')
@login_required
def statistics(file_id):
    file_ = model.File.query.get(file_id)

    stats = compute_file_statistics(file_id)
    metrics, entailment_counter, similarity_counter = stats

    return flask.render_template('statistics.html',
                                 filename=file_.name,
                                 entailment_counter=entailment_counter,
                                 similarity_counter=similarity_counter,
                                 metrics=metrics)

@bp.route('/annotator-statistics', methods=['POST'])
@login_required
def annotator_statistics():
    '''
    Return a view with statistics on annotators's raw agreement.
    '''
    ids_string = request.form['ids']
    annotator_ids = [int(val) for val in json.loads(ids_string)]
    annotator_ids.sort()

    agreements = compute_annotators_statistics(annotator_ids)
    annotators = get_annotators(annotator_ids)
    for annotator, agreement in zip(annotators, agreements):
        annotator.agreement = agreement

    return flask.render_template('annotator-statistics.html', users=annotators)

@bp.route('/overall-statistics', methods=['GET', 'POST'])
@login_required
def overall_statistics():
    '''
    Return a view with statistics over all pair annotations.
    '''
    if request.method == 'POST':
        # annotator ids were sent via post
        ids_string = request.form['ids']
        annotator_ids = json.loads(ids_string)
    else:
        annotator_ids = None

    try:
        stats = compute_overall_statistics(annotator_ids)
    except IndexError:
        if annotator_ids is not None:
            flask.flash(gettext('No pairs fully annotated by only the selected annotators.'), 'error')
            return flask.redirect(flask.url_for('view_users'))
        else:
            flask.flash(gettext('No pairs fully annotated.'), 'error')
            return flask.redirect(flask.url_for('rte.view_files'))

    metrics, entailment_counter, similarity_counter = stats

    return flask.render_template('statistics.html',
                                 entailment_counter=entailment_counter,
                                 similarity_counter=similarity_counter,
                                 metrics=metrics)

@bp.route('/annotate-pair')
@bp.route('/annotate-pair/<annotation_type>/<pair_id>/<class_>')
@login_required
def annotate_pair(annotation_type=None, pair_id=None, class_=None):
    '''
    Receive entailment or similarity annotation.
    '''
    if pair_id is None:
        # Ugly hack just to provide an endpoint with no arguments
        return ''

    annotator_id = current_user.id
    annotate_pair_rte(annotation_type, pair_id, annotator_id, class_)

    response_data = {'ok': True}
    return json.dumps(response_data)

@bp.route('/hints')
def hints():
    return flask.render_template('hints.html')

@bp.route('/download/', defaults={'file_id': None})
@bp.route('/download/<int:file_id>')
@login_required
def download_raw(file_id):
    if file_id is None:
        return ''

    file_ = model.File.query.get(file_id)
    file_data = generate_rte_file(file_id)
    return flask.send_file(file_data, as_attachment=True,
                           mimetype='text/xml',
                           attachment_filename=file_.name)

@bp.route('/view-files')
@login_required
def view_files():
    '''
    Just show the available files and operations on them.
    '''
    File = model.File
    files = File.query.filter_by(available=True, type='rte').all()
    for file_ in files:
        file_.load_metadata()

    return flask.render_template('choose-file.html', files=files)

@bp.route('/get-pair/<int:file_id>')
@login_required
def get_pair_for_review(file_id):
    '''
    Get a pair from the specified file to be reviewed by a user
    '''
    username = session['username']
    pair = get_unreviewed_pair(username, file_id)
    if pair is None:
        response_data = {'empty': True}
    else:
        response_data = {'id': pair.id,
                         't': pair.t,
                         'h': pair.h}

    response_data['ok'] = True
    return json.dumps(response_data)

@bp.route('/clean-file/', defaults={'file_id': None})
@bp.route('/clean-file/<int:file_id>')
@login_required
def clean_file(file_id):
    if file_id is None:
        return ''

    return flask.render_template('clean.html',
                                 file_id=file_id)

@bp.route('/clean-pair', methods=['POST'])
@login_required
def clean_pair():
    '''
    Make a change in a pair.
    '''
    if not current_user.may_edit_pair():
        return json.dumps({'ok': False})

    annotator_name = current_user.name
    operation = request.form['operation']
    pair_id = request.form['pair_id']

    if operation == 'delete':
        delete_pair(annotator_name, pair_id)
    elif operation == 'change':
        t = request.form['t']
        h = request.form['h']
        change_pair(pair_id, t, h, True)
    else:
        mark_as_reviewed(annotator_name, pair_id)

    return json.dumps({'ok': True})

@bp.route('/edit-pair', methods=['POST'])
@login_required
def edit_pair():
    '''Edit an existing pair, which was supposedly ready.'''
    if not current_user.may_edit_pair():
        return json.dumps({'ok': False})

    pair_id = request.form['pair_id']
    t = request.form['t']
    h = request.form['h']

    change_pair(pair_id, t, h, False)
    delete_annotations(pair_id)

    return json.dumps({'ok': True})

@bp.route('/flag-pair', methods=['POST'])
@login_required
def flag_pair():
    '''
    Function called during annotation to flag a pair for either editing or deletion.
    '''
    operation = request.form['operation']
    pair_id = request.form['pair_id']
    result_ok = flag_pair(pair_id, operation)

    if result_ok:
        return json.dumps({'ok': True})
    else:
        flask.flash('Invalid operation', 'error')
        return json.dumps({'ok': False})

