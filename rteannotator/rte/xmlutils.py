# -*- coding: utf-8 -*-

'''
Functions dealing with the XML RTE files.
'''

import os
from xml.etree import cElementTree as ET
from xml.dom import minidom

from rteannotator import db
from rteannotator.model import File
from model import RTEAnnotation, Pair

def process_rte_file(file_, root):
    '''
    Process a RTE XML tree, adding all pairs to the DB
    '''
    for xml_pair in root:
        pair = Pair(xml_pair, file_)
        db.session.add(pair)


def create_rte_file(pairs, entailment_dict=None, similarity_dict=None, use_db_id=False):
    '''
    Create an XML file with the given pairs. Entailment and similarity, if given,
    should be dictionaries mapping pair ids to the values.
    
    :param use_db_id: if True, use the DB id for the pairs. If False, pairs are id'd
        sequentially
    '''
    root = ET.Element('entailment-corpus')
    for i, pair in enumerate(pairs, 1):
        
        if entailment_dict is not None:
            entailment = entailment_dict[pair.id]
            if entailment == 2:
                # "second entails first". switch sentences and make it "first entails second"
                temp = pair.t
                pair.t = pair.h
                pair.h = temp
                entailment = 1
            
            if entailment == 1:
                entailment = 'Entailment'
            else:
                entailment = RTEAnnotation.rte_value_map[entailment]
        else:
            entailment = 'UNKNOWN'
        
        if similarity_dict is not None:
            similarity = str(similarity_dict[pair.id])
        else:
            similarity = 'UNKNOWN'
        
        id_ = pair.id if use_db_id else i
        xml_attribs = {'id': str(id_),
                       'entailment': entailment,
                       'similarity': similarity,
                       'vsm-similarity': str(pair.similarity),
                       'alpha1': str(pair.alpha1),
                       'alpha2': str(pair.alpha2)}
        
        xml_pair = ET.SubElement(root, 'pair', xml_attribs)
        xml_t = ET.SubElement(xml_pair, 't')
        xml_h = ET.SubElement(xml_pair, 'h')
        xml_t.text = pair.t
        xml_h.text = pair.h
    
    # pretty print
    xml_string = ET.tostring(root, 'utf-8')
    reparsed = minidom.parseString(xml_string)
    xml_string = reparsed.toprettyxml('    ', '\n', 'utf-8')
    
    return xml_string


