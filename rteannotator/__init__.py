# -*- coding: utf-8 -*-

'''
Definition of the Flask application object
'''

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_babel import Babel
from flask_breadcrumbs import Breadcrumbs
from flask_admin import Admin
import jinja2

import logging
import os
import itertools

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)
babel = Babel(app)
Breadcrumbs(app)

@app.template_global(name='zip_longest')
def _zip(*args, **kwargs):
    return itertools.izip_longest(*args, **kwargs)

login_manager = LoginManager()
login_manager.init_app(app)

if not app.debug:

    dir_name = os.path.dirname(__file__)

    log_path = os.path.join(dir_name, '..')
    error_path = os.path.join(log_path, 'errors.log')

    error_handler = logging.FileHandler(error_path)
    error_handler.setLevel(logging.WARNING)

    formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s '\
                                  '[in %(pathname)s:%(lineno)d]')
    error_handler.setFormatter(formatter)

    app.logger.addHandler(error_handler)

import views

# Add blueprints for RTE and CompTex
from rteannotator.rte.views import bp as rte_bp
from rteannotator.comptext.views import bp as comptext_bp
app.register_blueprint(rte_bp)
app.register_blueprint(comptext_bp)

# Admin page
from admin import setup_admin
admin = setup_admin(app, name='Anota!')

login_manager.login_view = 'login'
