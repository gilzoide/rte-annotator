# -*- coding: utf-8 -*-

'''
Functions dealing with the XML RTE files.
'''

import os
from xml.etree import cElementTree as ET

from rteannotator import db
from rteannotator.model import File
from rteannotator.rte.xmlutils import process_rte_file
from rteannotator.comptext.xmlutils import process_comptext_file

def parse_file(filepath):
    '''
    Load a XML file, and process it, taking care if file is RTE or CompText
    '''
    tree = ET.parse(filepath)
    root = tree.getroot()
    if root.tag == 'entailment-corpus':
        file_type = 'rte'
    elif root.tag == 'text-complexity-corpus':
        file_type = 'comptext'
    else:
        raise Exception("Invalid XML file")

    filename = os.path.basename(filepath)
    file_ = File(filename, file_type)
    file_.test = root.get('test') == 'true'
    try:
        file_.vsm = root.get('vsm')
    except:
        pass
    
    db.session.add(file_)
    if file_type == 'rte':
        process_rte_file(file_, root)
    else:
        process_comptext_file(file_, root)
    db.session.commit()


