# -*- coding: utf-8 -*-

import os
import json
import flask
import werkzeug
from flask import session, request, abort
from flask_login import login_required, login_user, logout_user, current_user
from flask_babel import gettext
from flask_breadcrumbs import register_breadcrumb
from sqlalchemy import update

from rteannotator.rte.model import *
from rteannotator import comptext
from rteannotator import model
from rteannotator import utils
from rteannotator import xmlutils
from rteannotator import app
from rteannotator import db
from rteannotator import babel
from config import LANGUAGES

@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(LANGUAGES.keys())

@app.route('/')
@register_breadcrumb(app, '.', 'Anota!')
@login_required
def index():
    return flask.render_template('home.html')

@app.route('/submit', methods=['POST'])
@login_required
def submit():
    '''
    File submission for annotation
    '''
    file_ = request.files['file']
    filename = werkzeug.utils.secure_filename(file_.filename)

    # check if file exists in DB
    File = model.File
    file_in_db = File.query.filter_by(name=filename).first()
    if file_in_db is not None:
        if file_in_db.test:
            db.session.delete(file_in_db)
            db.session.commit()
        else:
            flask.flash(gettext('There already exists a file with the same name. '\
                                'Are you sure this has not been submitted before?'))
            return flask.redirect(flask.url_for('index'))

    path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    file_.save(path)
    xmlutils.parse_file(path)
    flask.flash(gettext('File submited successfully'), 'info')
    return flask.redirect(flask.url_for('index'))

@app.route('/show-all')
@app.route('/show-all/<int:switch>')
@login_required
def toggle_show_all(switch=None):
    '''
    If switch is 1, set the annotator with the given id to see
    all pairs, including the ones already annotated by him/her.

    If switch is 0, only unannotated pairs will be shown.
    '''
    if switch is None:
        # Ugly hack just to provide an endpoint with no arguments
        return ''

    username = session['username']
    if switch == 0:
        switch = False
    elif switch == 1:
        switch = True

    toggle_show_all_pairs(username, switch)
    return flask.redirect(flask.url_for('annotate_file'))

@app.route('/download/', defaults={'file_id': None})
@app.route('/download/<int:file_id>')
@login_required
def download_raw(file_id):
    if file_id is None:
        return ''

    file_ = model.File.query.get(file_id)
    file_data = generate_rte_file(file_id)
    return flask.send_file(file_data, as_attachment=True,
                           mimetype='text/xml',
                           attachment_filename=file_.name)

@app.route('/download-annotated/', methods={'POST'})
@login_required
def download_annotated():
    num_pairs = int(request.form['num-pairs'])

    all_files = bool(request.form.get('all-files'))
    if not all_files:
        file_id = int(request.form['file-id'])
        filename = model.File.query.get(file_id).name
        filename = filename.replace('.xml', '-annotated.xml')
    else:
        file_id = None
        filename = 'rte-annotated.xml'

    agreeing_annotations = int(request.form['agreeing'])

    file_data = generate_annotated_file(file_id, num_pairs, agreeing_annotations)
    return flask.send_file(file_data, as_attachment=True,
                           mimetype='text/xml',
                           attachment_filename=filename)

@app.route('/view-users', methods=['GET', 'POST'])
@login_required
def view_users():
    if request.method == 'GET':
        users = Annotator.query.all()

        if current_user.may_see_all_statistics():
            # load number of annotations per user
            add_annotation_counts(users)
            comptext.utils.count_annotations(users)

        return flask.render_template('view-users.html', users=users)

    # POST request
    username = request.form['username']
    password = request.form['password']
    create_user(username, password)
    return flask.redirect(flask.url_for('view_users'))

@app.route('/login', methods=['GET', 'POST'])
def login():

    if request.method == 'GET':
        return flask.render_template('login.html')

    username = request.form['username']
    password = request.form['password']

    valid, msg = validate_login(username, password)
    if not valid:
        flask.flash(msg)
        return flask.render_template('login.html')

    session['username'] = username

    annotator = find_annotator(username)
    login_user(annotator)

    # redirect to the next page, if there is one
    next_url = flask.request.args.get('next')
    if not next_url:
        return flask.redirect(flask.url_for('index'))

    return flask.redirect(next_url)

@app.route('/logout')
@login_required
def logout():
    session.pop('username', None)
    logout_user()
    return flask.redirect(flask.url_for('login'))

@app.route('/change-password', methods=['GET', 'POST'])
@login_required
def change_password():
    if request.method == 'GET':
        return flask.render_template('change-password.html')

    # POST
    old_password = request.form['old-password']
    new_password = request.form['new-password']
    new_password_confirm = request.form['new-password-confirm']
    if new_password != new_password_confirm:
        flask.flash(gettext("New password not confirmed properly"), 'error')
        return flask.render_template('change-password.html')
    elif change_user_password(old_password, new_password):
        flask.flash(gettext('Password successfully changed'), 'info')
        return flask.redirect(flask.url_for('index'))
    else:
        flask.flash(gettext('Wrong password'), 'error')
        return flask.render_template('change-password.html')

def url_for_other_page(page):
    args = request.view_args.copy()
    args['page'] = page
    return flask.url_for(request.endpoint, **args)

app.jinja_env.globals['url_for_other_page'] = url_for_other_page


@app.route('/my-annotations')
def my_annotations():
    '''View for a user's annotations, be them RTE or CompText'''
    comptext = list(filter(lambda a: a.pair.test != True, current_user.comptex_annotations))
    return flask.render_template('my-annotations.html',
                                 rte=current_user.rte_annotations,
                                 comptext=comptext)


@app.route('/register-admin-fodao')
def register_admin_fodao():
    '''
    Register the first Admin for the system, with all privileges, ONLY ONCE.
    Send it's name in the query string {
    '''
    if model.Annotator.query.count() > 0:
        return abort(404)

    name = request.args.get('name')
    password = request.args.get('password')
    if name is None:
        return abort(400)

    admin_fodao = create_user(name, password)
    admin_fodao.type = 3
    db.session.commit()

    return flask.redirect(flask.url_for('login'))
