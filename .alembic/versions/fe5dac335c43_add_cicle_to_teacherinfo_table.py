"""Add cicle to TeacherInfo table

Revision ID: fe5dac335c43
Revises: 32270c8fde50
Create Date: 2017-04-27 12:01:30.489621

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'fe5dac335c43'
down_revision = '32270c8fde50'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('TeacherInfo',
        sa.Column('cicle', sa.Text)
    )


def downgrade():
    op.drop_column('TeacherInfo', 'cicle')
