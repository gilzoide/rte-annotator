"""Add pending comptext annotations

Revision ID: ac274091d0bd
Revises: 512c43a7eea9
Create Date: 2017-04-18 02:16:30.155362

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ac274091d0bd'
down_revision = '512c43a7eea9'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('PendingCompTextAnnotation',
        sa.Column('annotator_id', sa.Integer, sa.ForeignKey('annotator.id'), primary_key=True),
        sa.Column('pair_id', sa.Integer, sa.ForeignKey('CompTextPair.id'), primary_key=True),
    )


def downgrade():
    op.drop_table('PendingCompTextAnnotation')
