"""Add word count for texts

Revision ID: 0ecb000e7b9a
Revises: 41b1bafdddbe
Create Date: 2017-04-25 00:43:50.870231

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0ecb000e7b9a'
down_revision = '41b1bafdddbe'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('CompText', sa.Column('word_count', sa.Integer))


def downgrade():
    op.drop_column('CompText', 'word_count')
