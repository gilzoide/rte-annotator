"""Add size interval for texts

Revision ID: 41b1bafdddbe
Revises: c033ca17bcaf
Create Date: 2017-04-21 12:46:21.984053

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '41b1bafdddbe'
down_revision = 'c033ca17bcaf'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('CompText', sa.Column('size_interval', sa.Integer))


def downgrade():
    op.drop_column('CompText', 'size_interval')
