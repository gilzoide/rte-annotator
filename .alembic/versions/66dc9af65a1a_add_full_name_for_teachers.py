"""Add full name for Teachers

Revision ID: 66dc9af65a1a
Revises: 33218c865a3b
Create Date: 2017-04-28 15:56:39.255297

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '66dc9af65a1a'
down_revision = '33218c865a3b'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('TeacherInfo',
        sa.Column('full_name', sa.Text)
    )


def downgrade():
    op.drop_column('TeacherInfo', 'full_name')
