"""Add classifier level for texts

Revision ID: d039e29e9d9c
Revises: 66dc9af65a1a
Create Date: 2017-10-11 16:43:04.747003

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd039e29e9d9c'
down_revision = '66dc9af65a1a'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('CompText', sa.Column('classifier_level', sa.Integer))


def downgrade():
    op.drop_column('CompText', 'classifier_level')
