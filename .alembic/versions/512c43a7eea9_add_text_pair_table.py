"""Add text pair table

Revision ID: 512c43a7eea9
Revises: 58889c7fbdf2
Create Date: 2017-04-16 17:01:50.958449

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.orm import relationship


# revision identifiers, used by Alembic.
revision = '512c43a7eea9'
down_revision = '58889c7fbdf2'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('CompTextPair',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('text1_id', sa.Integer, sa.ForeignKey('comptext.id')),
        sa.Column('text2_id', sa.Integer, sa.ForeignKey('comptext.id')),
        sa.UniqueConstraint('text1_id', 'text2_id'),
    )


def downgrade():
    op.drop_table('CompTextPair')
