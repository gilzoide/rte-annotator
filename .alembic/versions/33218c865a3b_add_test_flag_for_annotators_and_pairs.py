"""Add test flag for annotators and pairs

Revision ID: 33218c865a3b
Revises: fe5dac335c43
Create Date: 2017-04-28 10:47:32.340171

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '33218c865a3b'
down_revision = 'fe5dac335c43'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('Annotator', sa.Column('test', sa.Boolean))
    op.add_column('CompTextPair', sa.Column('test', sa.Boolean))


def downgrade():
    op.drop_column('Annotator', 'test')
    op.drop_column('CompTextPair', 'test')
