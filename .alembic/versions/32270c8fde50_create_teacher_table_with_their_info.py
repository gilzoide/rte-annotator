"""Create Teacher table, with their info

Revision ID: 32270c8fde50
Revises: 0ecb000e7b9a
Create Date: 2017-04-25 11:20:39.950330

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '32270c8fde50'
down_revision = '0ecb000e7b9a'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('TeacherInfo',
        sa.Column('annotator_id', sa.Integer, sa.ForeignKey('Annotator.id'), primary_key=True),
        sa.Column('school', sa.Text),
        sa.Column('email', sa.Text),
        sa.Column('grade', sa.Text),
    )


def downgrade():
    op.drop_table('TeacherInfo')
