"""Include comment field in CompTextAnnotation

Revision ID: 684595a8b38c
Revises: 
Create Date: 2017-04-10 21:27:02.975020

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '684595a8b38c'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('CompTextAnnotation', sa.Column('comment', sa.Text))


def downgrade():
    op.drop_column('CompTextAnnotation', 'comment')
