"""Add title for CompTexts

Revision ID: c033ca17bcaf
Revises: ac274091d0bd
Create Date: 2017-04-20 10:36:42.885764

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c033ca17bcaf'
down_revision = 'ac274091d0bd'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('CompText', sa.Column('title', sa.Text))


def downgrade():
    op.drop_column('CompText', 'title')
