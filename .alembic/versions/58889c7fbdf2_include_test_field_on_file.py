"""Include test field on File

Revision ID: 58889c7fbdf2
Revises: 684595a8b38c
Create Date: 2017-04-11 12:16:51.487313

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '58889c7fbdf2'
down_revision = '684595a8b38c'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('File', sa.Column('test', sa.Boolean))


def downgrade():
    op.drop_column('File', 'test')
