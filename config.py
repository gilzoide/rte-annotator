# -*- coding: utf-8 -*-

'''
Configuration data for the web application.
'''

import os

# Statement for enabling the development environment
DEBUG = False

# Use a secure, unique and absolutely secret key for
# signing the data.
CSRF_SESSION_KEY = "\xa6\xbf\xad\x9az\xfb\x1a\xcbvpN<\xad,\xe14N\x00\xae\x94\n\x9cX\xe1"

# Secret key for signing cookies
SECRET_KEY = "\xbbu\xff\xeb\xdf\x81\xe4\xab\xf9\xcen\xe5\x14p\xdeUU\xe4f\xfdJ\x89\xd9,"

dirname = os.path.abspath(os.path.dirname(__file__))
UPLOAD_FOLDER = os.path.join(dirname, 'rteannotator', 'uploads')

# APPLICATION_ROOT = '/rte'

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(dirname, 'rte.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(dirname, 'db_repository')
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Supported Languages for translation
LANGUAGES = {
    'en': 'English',
    'pt': 'Português',
}
