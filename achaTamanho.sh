#!/bin/sh

if [[ $1 = "" ]]; then
	echo "É necessário nome da pasta!"
	exit -1
fi

arquivos=$(ls $1/*/*.txt)

for arq in $arquivos; do
	contagem=$(wc -w < $arq)
	dir=$(dirname $arq)
	base=$(basename $arq)
	
	# muda o nome do arquivo =]
	mv $arq $dir/$(printf "%04d" $contagem)_$base
done
