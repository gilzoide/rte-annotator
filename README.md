Anota!
======
Plataforma de anotação Anota!

Atualmente, realiza anotação de Inferência Textual (RTE) e de
Complexidade textual.


Preparação
----------
Dependências:
- [Python 2](http://python.org/)
- [R](https://www.r-project.org/)
- alguma *engine* de banco de dados SQL (por padrão, [SQLite](https://sqlite.org/))

Instale as dependências do *Python* (opcionalmente em um
[virtualenv](https://virtualenv.pypa.io/)):

	$ pip install -r requirements.txt

Instale o pacote `irr` do *R*:

```r
# dentro do shell do R
i> install.packages("irr")
```

(Opcional) Configure a base de dados a ser usada através das [configurações do
Flask-SQLAlchemy](http://flask-sqlalchemy.pocoo.org/2.3/config/) no arquivo
`config.py`.

E crie a base de dados:

	$ python dbsetup.py


Para rodar
----------

	$ python run.py


Adaptando o sistema
-------------------
Veja o [tutorial](tutorial.md) para adaptar a plataforma com novos subsistemas
de anotação.

