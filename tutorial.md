Tutorial
========
Este documento dará um overview do sistema como um todo, assim como quais os
passos necessários para se criar um novo subsistema de anotação. Sugestões
sobre refatorações interessantes para facilitar esse processo também é dada no
final.


## 1. Crie um novo pacote
-------------------------
Cada subsistema de anotação tem seu próprio [pacote python](https://docs.python.org/2.7/tutorial/modules.html#packages).
Isso ajuda na modulrização do sistema (e sanidade do desenvolvedor), já que
esses subsistemas não precisam (ou pelo menos não deveriam) se comunicar. Para
isso, basta criar uma nova pasta dentro de `rteannotator` com o nome desejado e
criar um arquivo `__init__.py` (necessário para *python 2* reconhecer como um
pacote) dentro dela:

    $ mkdir rteannotator/<nome do pacote>
    $ touch rteannotator/<nome do pacote>/__init__.py


## 2. Crie um Blueprint
-----------------------
[Blueprints](http://flask.pocoo.org/docs/0.12/blueprints/) são um jeito bacana
de modularizar uma aplicação *flask*. Crie uma para seu subsistema e registre-o
no arquivo `rteannotator/__init__.py` após a criação do `app`:

```python
# rteannotator/<nome do pacote>/views.py
from flask import Blueprint
<nome da blueprint> = Blueprint("Nome do subsistema", __name__, url_prefix="/prefixo")

# rteannotator/__init__.py
from rteannotator.<nome do pacote> import <nome da blueprint>
app.register_blueprint(<nome da blueprint>)
```


## 3. Crie um link para seu subsistema a partir da página inicial
-----------------------------------------------------------------
Para seu subsistema ser acessível para os usuários, é bacana prover um *link*
para ele. Edite o arquivo `rteannotator/templates/home.html` adicionando a URL
e opcionalmente um texto explicativo:

```html
<div class="subsystems-table">
    <!-- ... -->
    <div class="subsystem">
        <div class="text">
            <h2><a href="{{ url_for("meu_subsistema_de_anotação.index") }}">Nome do Subsistema</a></h2>
            Ponha aqui uma descrição explicando seu sistema.
        </div>
    </div>
    <!-- ... -->
</div>
```


## 4. Registre a entrada de dados via arquivo XML (opcional)
------------------------------------------------------------
Na janela inicial, para usuários com permissões de administrador, há um
formulário para envio de arquivos de entrada. Para usar desse mecanismo já
pronto, basta adicionar a funcionalidade extra na função
`rteannotator.xmlutils.parse_file`, verificando qual a *tag* raíz e
processando-a:

```python
def parse_file(filepath):
    # ...
    elif root.tag == 'ponha-aqui-a-tag-desejada':
        file_type = 'meu tipo de anotação'
    # ...
    elif file_type == 'meu tipo de anotação':
        # Executar processamento do arquivo de entrada, possivelmente
        # implementado em seu pacote
    # ...
```


## 5. Faça mudanças ao banco de dados
-------------------------------------
Para alterar a estrutura da base de dados, com novas tabelas, relações,
atributos, etc., é usada a ferramenta [alembic](https://pypi.python.org/pypi/alembic).
Leia a [documentação da ferramenta](http://alembic.zzzcomputing.com/en/latest/)
para mais informações (a parte importante é sobre [scripts de
migração](http://alembic.zzzcomputing.com/en/latest/tutorial.html#create-a-migration-script),
já que já há uma configuração preparada).


Sugestão de melhorias na modularização
--------------------------------------
- Poder escolher na configuração quais subsistemas serão carregados. Além de
  diminuir a carga de memória e processamento caso um subsistema não seja
  carregado, isso facilitaria a implementação da próxima sugestão.
- Não precisar duplicar código no registro de Blueprints, entrada de dados
  via XML, páginas de administração, e outras funcionalidades comuns. Para
  isso, deveria ser criado um protocolo de nomenclatura de objetos/funções
  comuns, possivelmente usando POO e polimorfismo. Ter uma lista dos
  subsistemas a serem carregados facilitaria essa tarefa, basta um *loop* para
  registrar cada funcionalidade dessas por subsistema.
- Mover funcionalidades da anotação *RTE* do pacote `rteannotator` pro
  `rteannotator.rte`.
- Renomear o pacote `rteannotator` para `anota`, já que esse é o novo nome do
  projeto.
- Criar pastas separadas para *templates* por subsistema.

